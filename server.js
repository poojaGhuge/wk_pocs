const express = require('express');
const bodyParser = require('body-parser');
const app = express();
const cors = require('cors');
const port = 6005;
const path = require('path');
var CronJob = require('cron').CronJob;
process.env['NODE_TLS_REJECT_UNAUTHORIZED'] = '0';

var appReportsRoutes = require('./routes/reports-routes');
var loadTender = require('./routes/load-tender.route');
const driver = require('./routes/driver.route');
const carrier = require('./routes/carrier.route');
const readpdf = require('./routes/readpdf.route');
const checkcall = require('./routes/checkcall.route');


app.use(cors())
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json({ limit: 1024102420, type: 'application/json' }));
app.use(function (req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, access_token");
  // next();
  // intercept OPTIONS method
  if ('OPTIONS' == req.method) {
    res.send(200);
  } else {
    next();
  }
});

var customDir = "F:\\";
app.use(express.static(path.join(customDir, 'bol\\')));
app.use(express.static(path.join(customDir, 'pod\\')));
app.use(express.static(path.join(customDir, 'receipt\\')));
app.use(express.static(path.join(customDir, 'inspectionsheet\\')));
app.use(express.static(path.join(customDir, 'bond\\')));
app.use(express.static(path.join(customDir, 'cci\\')));
app.use(express.static(path.join(customDir, 'lastimage\\')));
app.use(express.static(path.join(customDir, 'faults\\')));
app.use(express.static(path.join(customDir, 'podesign\\')));
app.use(express.static(path.join(customDir, 'hiddenimg\\')));
app.use(express.static(path.join(customDir, 'esign\\')));
app.use(express.static(path.join(customDir, 'scales\\')));
app.use(express.static(path.join(customDir, 'aci\\')));

app.use('/', express.static(path.join(__dirname, 'public')));

app.use('/reports', appReportsRoutes);
app.use('/load-tender', loadTender);
app.use('/driver', driver);
app.use('/carrier', carrier);
app.use('/readpdf', readpdf);
app.use('/checkcall', checkcall);

app.use('/', (req, res) => {
  res.send({
    msg: "success"
  });
})


// CronJobs for get fleet Manager data
var CronJobs = require('./lib/CronJobs');
CronJobs.getfleetMangerDataConnect();


app.get('/', (request, response) => {
  response.json({ info: 'Node.js, Express, and Postgres API' })
})

app.listen(port, () => {
  console.log(`App running on port ${port}.`)
})



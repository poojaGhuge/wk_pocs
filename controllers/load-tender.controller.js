var sql = require("mssql");
var moment = require('moment');

const getCommentsOnTrip = (request, response) => {
    var request = new sql.Request();
    request.query(`
            SELECT RealName,[FleetTraining].[dbo].[Trip].TripID,[FleetTraining].[dbo].[Trip].Comment
  FROM [FleetTraining].[dbo].[User] inner join [FleetTraining].[dbo].[Trip] on 
  [FleetTraining].[dbo].[Trip].CreatedBy=[FleetTraining].[dbo].[User].UserID
  where len([FleetTraining].[dbo].[Trip].Comment) !=0 order by TripID desc
            `, function (err, recordset) {
        if (err) {
            console.log('error in fetching data ', err)
            response.json({ success: false, data: 'Records not found' })
        } else {
            response.json({ success: true, data: recordset })
        }
    });
}

const paymentTerms = (request, response) => {
    try {
        var request = new sql.Request();
        request.query(`select distinct terms from [Order]`, function (err, recordset) {
            if (err) {
                response.json({ success: false, data: 'Records not found' })
            } else {
                response.json({ success: true, data: recordset.recordset })
            }
        });
    } catch (error) {
        response.json({ success: false, data: error })
    }

}

const categories = (request, response) => {
    try {
        var request = new sql.Request();
        request.query(`select poolid,poolname from templatepool where pooltype='Probill' and status='Active'`, function (err, recordset) {
            if (err) {
                response.json({ success: false, data: 'Records not found' })
            } else {
                response.json({ success: true, data: recordset.recordset })
            }
        });
    } catch (error) {
        response.json({ success: false, data: error })
    }

}

const customBrokers = (req, response) => {
    try {
        var searchTerm = req.params.searchTerm;
        var request = new sql.Request();
        request.query("select brokerid, broker from customsbroker where status='Active' and broker like '%" + searchTerm + "%'", function (err, recordset) {
            if (err) {
                response.json({ success: false, data: 'Records not found' })
            } else {
                response.json({ success: true, data: recordset.recordset })
            }
        });
    } catch (error) {
        response.json({ success: false, data: error })
    }

}

const pickupScheduleTypes = (req, response) => {
    try {
        const data = [
            { pickupScheduleTypeId: 'Available', value: 'Ready for Pickup' },
            { pickupScheduleTypeId: 'T.B.A.', value: 'Call for Appointment' },
            { pickupScheduleTypeId: 'Appointment', value: 'Scheduled Appointment' },
            { pickupScheduleTypeId: 'Window', value: 'Scheduled Pickup Window' },
            { pickupScheduleTypeId: 'Appointment', value: 'Rescheduled Appointment' },
            { pickupScheduleTypeId: 'Window', value: 'Rescheduled Pickup Window' },
            { pickupScheduleTypeId: 'Available', value: 'Rescheduled Pickup On' },
        ]
        response.json({
            success: true,
            data: data
        })
    } catch (error) {
        response.json({ success: false, data: error })
    }

}

const deliveryScheduleTypes = (req, response) => {
    try {
        const data = [
            { deliveryScheduleTypeId: 'Available', value: 'No Later Than' },
            { deliveryScheduleTypeId: 'T.B.A.', value: 'Call for Appointment' },
            { deliveryScheduleTypeId: 'Appointment', value: 'Scheduled Appointment' },
            { deliveryScheduleTypeId: 'Window', value: 'Scheduled Delivery Window' },
            { deliveryScheduleTypeId: 'Appointment', value: 'Resheduled Appointment' },
            { deliveryScheduleTypeId: 'Window', value: 'Rescheduled Delivery Window' },
            { deliveryScheduleTypeId: 'Available', value: 'Rescheduled For No Later Than' },
        ]
        response.json({
            success: true,
            data: data
        })
    } catch (error) {
        response.json({ success: false, data: error })
    }
}

const divisions = (req, response) => {
    try {
        var request = new sql.Request();
        request.query("select divisionid,divcode,divisionname from Division where status='Active'", function (err, recordset) {
            if (err) {
                response.json({ success: false, data: 'Records not found' })
            } else {
                response.json({ success: true, data: recordset.recordset })
            }
        });
    } catch (error) {
        response.json({ success: false, data: error })
    }
}

const salesReps = (req, response) => {
    try {
        var request = new sql.Request();
        request.query("select salesrepid,salesrepcode,salesrepname from salesrep where status='Active'", function (err, recordset) {
            if (err) {
                response.json({ success: false, data: 'Records not found' })
            } else {
                response.json({ success: true, data: recordset.recordset })
            }
        });
    } catch (error) {
        response.json({ success: false, data: error })
    }
}

const currencies = (req, response) => {
    try {
        const data = [
            { currencyId: 'C', value: 'CAD' },
            { currencyId: 'U', value: 'USD' },
        ]
        response.json({
            success: true,
            data: data
        })
    } catch (error) {
        response.json({ success: false, data: error })
    }
}

const billingCompany = (req, response) => {
    try {
        var customerId = req.params.customerId
        var request = new sql.Request();
        request.query("select billingid,company,city from CustomerBilling where status='Active' and billingid=(select billingid from CustomerBillingrelated where customerid=" + customerId + ")", function (err, recordset) {
            if (err) {
                response.json({ success: false, data: 'Records not found' })
            } else {
                response.json({ success: true, data: recordset.recordset })
            }
        });
    } catch (error) {
        response.json({ success: false, data: error })
    }
}

const equipmentTypes = (req, response) => {
    try {
        var request = new sql.Request();
        request.query("select equiptypeid,equipdesc from equipmenttype where equiptype='Trailer'", function (err, recordset) {
            if (err) {
                response.json({ success: false, data: 'Records not found' })
            } else {
                response.json({ success: true, data: recordset.recordset })
            }
        });
    } catch (error) {
        response.json({ success: false, data: error })
    }
}

const locations = (req, response) => {
    try {
        var searchTerm = req.params.searchTerm;
        var request = new sql.Request();
        request.query("select locid, CONCAT(Location, '-', City) locationName, Address1 address,PostalCode,j.Name state,j.code stateCode from location l join Jurisdiction j on l.province = j.jurid where status='Active' and location like '%" + searchTerm + "%'", function (err, recordset) {
            if (err) {
                response.json({ success: false, data: 'Records not found' })
            } else {
                response.json({ success: true, data: recordset.recordset })
            }
        });
    } catch (error) {
        response.json({ success: false, data: error })
    }
}

const units = (req, response) => {
    try {
        const quanityUnits = ['skids', 'pcs', 'pallets', 'cartons', 'crates', 'boxes', 'cases', 'feet', 'yards', 'meters', 'cu.ft.', 'cu.yards', 'scm', 'm3', 'scf', 'ccf', 'gal.', 'imp.gal.', 'litres', 'quarts', 'imp.quarts', 'truck load', 'bags', 'bales', 'barrels', 'bins', 'buckets', 'bundles', 'carts', 'casks', 'cylinders', 'drums', 'hampers', 'kegs', 'pails', 'totes', 'racks', 'reels', 'rolls'];
        const weightUnits = ['lbs', 'kgs', 'tons', 'imp.tons', 'tonnes'];

        response.json({
            success: true,
            data: {
                quanityUnits: quanityUnits,
                weightUnits: weightUnits
            }
        })
    } catch (error) {
        response.json({ success: false, data: error })
    }
}

const customer = (req, response) => {
    try {
        var searchTerm = req.params.searchTerm;
        var request = new sql.Request();
        request.query("select customerid, company customerName,City from customer where status='Active' and Company like '%" + searchTerm + "%'", function (err, recordset) {
            if (err) {
                response.json({ success: false, data: 'Records not found' })
            } else {
                response.json({ success: true, data: recordset.recordset })
            }
        });
    } catch (error) {
        response.json({ success: false, data: error })
    }
}

const accChargeTypes = (req, response) => {
    try {
        var request = new sql.Request();
        request.query("select code,chargeType,description from acccharge where Status='Active'", function (err, recordset) {
            if (err) {
                response.json({ success: false, data: 'Records not found' })
            } else {
                response.json({ success: true, data: recordset.recordset })
            }
        });
    } catch (error) {
        response.json({ success: false, data: error })
    }
}

const getPickupDeliveryScheduleTypesName = (mode, typeId, isRe) => {
    if (mode === 'delivery') {
        if (isRe) {
            var res
            [
                { deliveryScheduleTypeId: 'Appointment', value: 'RESCHEDULED APPOINTMENT' },
                { deliveryScheduleTypeId: 'Window', value: 'RESCHEDULED DELIVERY WINDOW' },
                { deliveryScheduleTypeId: 'Available', value: 'RESCHEDULED FOR NO LATER THAN' },
            ].forEach(_ => {
                if (_.deliveryScheduleTypeId == typeId) {
                    res = _.value
                    return;
                }
            });
            return res
        }

        var res;
        [
            { deliveryScheduleTypeId: 'Available', value: 'NO LATER THAN' },
            { deliveryScheduleTypeId: 'T.B.A.', value: 'CALL FOR APPOINTMENT' },
            { deliveryScheduleTypeId: 'Appointment', value: 'SCHEDULED APPOINTMENT' },
            { deliveryScheduleTypeId: 'Window', value: 'SCHEDULED DELIVERY WINDOW' },
        ].forEach(_ => {
            if (_.deliveryScheduleTypeId == typeId) {
                res = _.value
                return;
            }
        });
        return res
    }
    else {
        if (isRe) {
            var res;
            [
                { pickupScheduleTypeId: 'Appointment', value: 'RESCHEDULED APPOINTMENT' },
                { pickupScheduleTypeId: 'Window', value: 'RESCHEDULED PICKUP WINDOW' },
                { pickupScheduleTypeId: 'Available', value: 'RESCHEDULED PICKUP ON' },
            ].forEach(_ => {
                if (_.pickupScheduleTypeId == typeId) {
                    res = _.value
                    return;
                }
            });
            return res
        }
        var res
        [
            { pickupScheduleTypeId: 'Available', value: 'READY FOR PICKUP' },
            { pickupScheduleTypeId: 'T.B.A.', value: 'CALL FOR APPOINTMENT' },
            { pickupScheduleTypeId: 'Appointment', value: 'SCHEDULED APPOINTMENT' },
            { pickupScheduleTypeId: 'Window', value: 'SCHEDULED PICKUP WINDOW' },
        ].forEach(_ => {
            if (_.pickupScheduleTypeId == typeId) {
                res = _.value
                return;
            }
        });
        return res
    }
}

const checkOrderExists = async (req, res) => {
    var poNo = req.params.poNo;
    var customerId = +req.params.customerId;
    var request = new sql.Request();
    var checkPONumber = await request.query("select o.orderNo,p.probillno from [order] o join probill p on o.orderno=p.orderno where o.cancelled=0 and pono='" + poNo + "' and customerid=" + customerId);
    if (checkPONumber.recordset && checkPONumber.recordset.length > 0) {
        res.json({
            success: true,
            orderNo: checkPONumber.recordset[0].orderNo,
            probillNo: checkPONumber.recordset[0].probillno,
        })
    } else {
        res.json({ success: true, orderNo: null, probillNo: null })
    }

}

const insertLoadTender = async (req, response) => {
    try {
        var request = new sql.Request();
        var data = req.body;
        let payload = {
            userId: -1,
            terms: data.terms,
            customerId: data.customerId,
            customerName: data.customerName,
            billToId: data.billToId,
            divisionId: data.divisionId,
            salesRepId: data.salesRepId,
            salesrepName: data.salesrepName,
            currency: data.currency,
            rateCurrency: data.rateCurrency,
            rateExchange: data.rateExchange,
            poNo: data.poNo,
            callersName: data.callersName,
            weightUnits: data.weightUnits,
            qtyUnits: data.qtyUnits,
            instructions: data.instructions,
            invoiceTotal: +data.invoiceTotal,
            waitingTime: +data.waitingTime,
            weight: data.weight,
            freightSummary: data.freightSummary,
            ordDistance: data.ordDistance,
            dispatchAlert: data.dispatchAlert,
            categoryId: data.categoryId,
            locationId: data.locationId,
            equipTypeId: data.equipTypeId,
            codCurrency: data.currency,
            hazMat: data.hazMat,
            truckload: data.truckload,
            farmOut: data.farmOut,
            shipperLocID: data.shipperLocID,
            pickupLocID: data.pickupLocID,
            pick_Scheduling: data.pick_Scheduling,
            pick_DT1: data.pick_DT1,
            pick_DT2: data.pick_DT2,
            pick_notes: data.pick_notes,
            consigneeLocId: data.consigneeLocId,
            deliveryLocID: data.deliveryLocID,
            del_Scheduling: data.del_Scheduling,
            del_DT1: data.del_DT1,
            del_DT2: data.del_DT2,
            del_notes: data.del_notes,
            customsBrokerId: data.customsBrokerId,
            puNumber: data.puNumber,
            productDesc: data.productDesc,
            delNumber: data.delNumber,
            chargeCode: data.chargeCode,
            chargeDesc: data.chargeDesc,
            chargeType: data.chargeType,
            moveDesc: data.moveDesc,
            pick_schedulingName: data.pick_schedulingName,
            del_schedulingName: data.del_schedulingName,
            qst: 1,
            qstAmount: 0,
            hst: 1,
            hstAmount: 0,
            gst: 1,
            gstAmount: 0,
            fsRate: 0,
            fsAmount: 0,
            freightAudit: 0,
            invoiceToPrint: 0,
            version: 0,
            cancelled: 0,
            historyNotes: 0,
            ediComplete: 0,
            multiPro: 0,
            editBy: -1,
            faOrder: 0,
            faUpdated: 1,
            fsApplied: 0,
            fsCharged: 0,
            qty: data.qty,
            ordMeasure: 'M',
            acctManagerId: -1,
            orderRouteMethod: 'Auto Route',
            taxAmount: 0,
            fsAppliedAs: 0,
            fsMeasure: '%',
            reverseCalculateFuelSurcharge: 0,
            // Probill
            status: 'PENDING',
            cStatus: 'PENDING',
            expedite: 0,
            customs_ImporterID: 0,
            customs_CSA: 0,
            customs_FAST: 0,
            customs_PostAudit: 0,
            EDIProbill: 0,
            revFuel: 0,
            totalCost: 0,
            rateParticulars: 'Flat Rated',
            revenuePatched: 1,
            tripRevenue: 0,
            missedPick: 0,
            FAUpdated: 1,
            customs_ReleaseType: 'PAPS',
            customs_SCAC: 'WKTV',
            driverTraining: 0,
            shipmentSelected: 1,
            pmitProbill: 0,
            FAEventsUpdated: 0,
            pNotes: data.pick_notes == '' || data.pick_notes == null ? 0 : 1,
            dNotes: data.del_notes == '' || data.del_notes == null ? 0 : 1,
            totalCostCAD: 0,
            totalCostUSD: 0,
            customs_InBondPartyID: 0,
            automateX6Status: 0,
            customs_BrokerToFile: 0,
            EDICustomProbill: 0,
            deliveryTracked: 0,
            freightSnapUsed: 0,
            EDIPreventStatuses: 0,
            omitProbill: 0
        }
        payload.dispatchAlert = payload.dispatchAlert ? payload.dispatchAlert.replace("'", "''") : payload.dispatchAlert;
        payload.del_notes = payload.del_notes ? payload.del_notes.replace("'", "''") : payload.del_notes;
        payload.pick_notes = payload.pick_notes ? payload.pick_notes.replace("'", "''") : payload.pick_notes;
        payload.instructions = payload.instructions ? payload.instructions.replace("'", "''") : payload.instructions;
        payload.callersName = payload.callersName ? payload.callersName.replace("'", "''") : payload.callersName;
        payload.productDesc = payload.productDesc ? payload.productDesc.replace("'", "''") : payload.productDesc;
        var checkPONumber = await request.query("select o.orderNo,p.probillno from [order] o join probill p on o.orderno=p.orderno where o.cancelled=0 and pono='" + payload.poNo + "' and customerid=" + payload.customerId);
        if (checkPONumber.recordset && checkPONumber.recordset.length > 0) {
            response.json({
                success: true,
                message: 'Order Already Exists. Order No ' + checkPONumber.recordset[0].orderNo + ' Probill No ' + checkPONumber.recordset[0].probillno,
                orderNo: checkPONumber.recordset[0].orderNo,
                probillNo: checkPONumber.recordset[0].probillno,
            })
        } else {
            var query = "INSERT INTO [dbo].[Order](Orderno,OrderDT,UserID,Terms,CustomerID,BilltoID,DivisionID,SalesRepID,Commission,Currency,RateCurrency,PONo,CallersName,WeightUnits,QtyUnits,Instructions,QST,QSTAmount,HST,HSTAmount,GST,GSTAmount,InvoiceTotal,FSBase,FSRate,FSAmount,FreightAudit,InvoiceToPrint,Version,WaitingTime,Cancelled,HistoryNotes,EDIComplete,MultiPro,EditBy,EditDate,FAOrder,FAUpdated,FSApplied,FSCharged,FreightSummary,OrdDistance,OrdMeasure,ProbillCount,AcctManagerID,DispatchAlert,OrderRouteMethod,TaxAmount,FSMeasure,ReverseCalculateFuelSurcharge)VALUES((Select max(orderno)+1 from [order]),getdate()," + payload.userId + ",'" + payload.terms + "'," + payload.customerId + "," + payload.billToId + "," + payload.divisionId + "," + payload.salesRepId + ",0,'" + payload.currency + "','" + payload.currency + "','" + payload.poNo + "','" + payload.callersName + "','" + payload.weightUnits + "','" + payload.qtyUnits + "','" + payload.instructions + "'," + payload.qst + "    ," + payload.qstAmount + "," + payload.hst + "," + payload.hstAmount + "," + payload.gst + "," + payload.gstAmount + "," + payload.invoiceTotal + "," + payload.invoiceTotal + "," + payload.fsRate + "," + payload.fsAmount + "," + payload.freightAudit + "," + payload.invoiceToPrint + "," + payload.version + "," + payload.waitingTime + "," + payload.cancelled + "," + payload.historyNotes + "," + payload.ediComplete + "," + payload.multiPro + "," + payload.userId + ",getdate()," + payload.faOrder + "," + payload.faUpdated + "," + payload.fsApplied + "," + payload.fsCharged + ",'" + payload.freightSummary + "'," + payload.ordDistance + ",'" + payload.ordMeasure + "',1," + payload.userId + ",'" + payload.dispatchAlert + "','" + payload.orderRouteMethod + "'," + payload.taxAmount + ",'" + payload.fsMeasure + "'," + payload.reverseCalculateFuelSurcharge + ") ;Select max(orderno) orderNo from [order]";
            var insertOrder = await request.query(query);
            var orderNo = insertOrder.recordset && insertOrder.recordset[0].orderNo ? insertOrder.recordset[0].orderNo : 0;
            if (orderNo !== 0) {
                var proQuery = "INSERT INTO [dbo].[Probill](probillno,OrderNo,Status,CStatus,CategoryID,LocationID,EquipTypeID,UserID,CODCurrency,HazMat,Truckload,FarmOut,Expedite,ShipperLocID,PickupLocID,Pick_Scheduling,Pick_DT1,Pick_Notes,ConsigneeLocID,DeliveryLocID,Del_Scheduling,Del_DT1,Del_Notes,Customs_ImporterID,Customs_CSA,Customs_FAST,Customs_PostAudit,PUNumber,FreightSummary,Distance,Measure,Revenue,Cancelled,EDIProbill,RevMiles,RevFuel,RevDriver,TotalCost,FlatPaymentCurrency,RateParticulars,RevenuePatched,TripRevenue,RevCommission,MissedPick,FAUpdated,Customs_ReleaseType,Customs_SCAC,DriverTraining,ProductDesc,ShipmentSelected,Pick_Date1,Del_Date1,OmitProbill,FAEventsUpdated,DelNumber,Pick_Schedule,Del_Schedule,PNotes,DNotes,TotalCostCAD,TotalCostUSD,Customs_InBondPartyID,AutomateX6Status,Customs_BrokerToFile,EDICustomProbill,DeliveryTracked,FreightSnapUsed,EDIPreventStatuses)VALUES((Select max(probillno)+1 from [probill])," + orderNo + ", '" + payload.status + "', '" + payload.cStatus + "', " + payload.categoryId + ", " + payload.locationId + ", " + payload.equipTypeId + "," + payload.userId + " , '" + payload.currency + "', " + payload.hazMat + ", " + payload.truckload + ", " + payload.farmOut + ", " + payload.expedite + ", " + payload.shipperLocID + ", " + payload.pickupLocID + ", '" + payload.pick_Scheduling + "', '" + payload.pick_DT1 + "',  '" + payload.pick_notes + "', " + payload.consigneeLocId + ", " + payload.deliveryLocID + ", '" + payload.del_Scheduling + "', '" + payload.del_DT1 + "', '" + payload.del_notes + "',  " + payload.customs_ImporterID + ", " + payload.customs_CSA + ", " + payload.customs_FAST + ", " + payload.customs_PostAudit + ", '" + payload.puNumber + "', '" + payload.freightSummary + "', " + payload.ordDistance + ", '" + payload.ordMeasure + "', " + payload.invoiceTotal + ", " + payload.cancelled + ", " + payload.EDIProbill + ", " + payload.ordDistance + ", " + payload.revFuel + ", " + payload.invoiceTotal + ", " + payload.totalCost + ", '" + payload.currency + "', '" + payload.rateParticulars + "', " + payload.revenuePatched + ", " + payload.tripRevenue + ", " + payload.invoiceTotal + "," + payload.missedPick + " , " + payload.faUpdated + ", '" + payload.customs_ReleaseType + "', '" + payload.customs_SCAC + "', " + payload.driverTraining + ", '" + payload.productDesc + "', " + payload.shipmentSelected + ", '" + payload.pick_DT1 + "', '" + payload.del_DT1 + "', " + payload.omitProbill + ", " + payload.FAEventsUpdated + ", '" + payload.delNumber + "',  '" + payload.pick_Scheduling + "', '" + payload.del_Scheduling + "', " + payload.pNotes + ", " + payload.dNotes + ", " + payload.totalCostCAD + ", " + payload.totalCostUSD + ", " + payload.customs_InBondPartyID + ", " + payload.automateX6Status + ", " + payload.customs_BrokerToFile + ", " + payload.EDICustomProbill + ", " + payload.deliveryTracked + ", " + payload.freightSnapUsed + "," + payload.EDIPreventStatuses + "); Select max(probillno) probillNo from [probill]";
                var insertProbill = await request.query(proQuery);
                var probillNo = insertProbill.recordset && insertProbill.recordset[0].probillNo ? insertProbill.recordset[0].probillNo : 0;
                if (probillNo !== 0) {
                    if (payload.rateExchange) {
                        var queryUpdateOrder = "update [order] set rateexchange=" + payload.rateExchange + " where orderno=" + orderNo;
                    }
                    if (payload.pick_DT2) {
                        var updateProbill = await request.query("update probill set Pick_DT2='" + payload.pick_DT2 + "', Pick_Date2='" + payload.pick_DT2 + "' where probillno=" + probillNo);
                    }
                    if (payload.del_DT2) {
                        var updateProbill = await request.query("update probill set del_DT2='" + payload.del_DT2 + "', del_Date2='" + payload.del_DT2 + "' where probillno=" + probillNo);
                    }
                    if (payload.customsBrokerId) {
                        var query = "update probill set Customs_BrokerID='" + payload.customsBrokerId + "' where probillno=" + probillNo;
                        var updateProbill = await request.query(query);
                    }
                    var queryUpdateOrder = "update [order] set firstproid=" + probillNo + ", lastproid=" + probillNo + " where orderno=" + orderNo;
                    await request.query(queryUpdateOrder);
                    var queryInsertProbillSegment = "INSERT INTO [dbo].[ProbillSegment](ProbillNo,Segment,FDA)VALUES(" + probillNo + ",1,0);";
                    await request.query(queryInsertProbillSegment);
                    var queryInsertProBody = "INSERT INTO [dbo].[ProBody](ProbillNo,Segment,Line,BOLNo,Particulars,EDIProBody,SatProBodyID,AutoCalculated,ReadOnly)VALUES(" + probillNo + ",1,1,'" + payload.poNo + "','" + payload.productDesc + "',0,0,0,0);";
                    await request.query(queryInsertProBody);
                    if (payload.qty) {
                        var queryUpdateProBody = "update probody set Qty=" + payload.qty + ",QtyUnits='" + payload.qtyUnits + "' where probillno=" + probillNo;
                        await request.query(queryUpdateProBody);
                    }
                    if (payload.weight) {
                        var queryUpdateProBody = "update probody set Weight=" + payload.weight + ",WeightUnits='" + payload.weightUnits + "' where probillno=" + probillNo;
                        await request.query(queryUpdateProBody);
                    }
                    var queryInsertOrderCharge = "INSERT INTO [dbo].[OrderCharge](OrderNo,Line,ChargeCode,ChargeDesc,ChargeType,ApplyAs,Qty,UnitPrice,Maximum,Commission,PayDriver,ApplyFuelSurcharge,QST,HST,GST,TotalAmount,ChangedBy,ChangedOn,TaxRate1,TaxRate2,PiggyBack2,OverRide1,OverRide2,OldTotal,OldUnitPrice)VALUES(" + orderNo + ",1,'" + payload.chargeCode + "','" + payload.chargeDesc + "','" + payload.chargeType + "','$',1," + payload.invoiceTotal + ",0,1,1,1,0,0,0," + payload.invoiceTotal + "," + payload.userId + ",getDate(),0,0,0,0,0," + payload.invoiceTotal + "," + payload.invoiceTotal + ");";
                    await request.query(queryInsertOrderCharge);
                    var queryInsertOrderHistory = "INSERT INTO [dbo].[OrderHistory](OrderNo,DateRecorded,RecordedBy,HistoryItem,NoteType,FieldName)VALUES(" + orderNo + ",getDate()," + payload.userId + ",'ORDER WAS CREATED AND PREVIEWED.','Audit','ORDER CREATED');";
                    await request.query(queryInsertOrderHistory);
                    /* Other Auto Impacted Tables and Views
                     VWActiveShipments
                     ProbillRpt
                     vwRevReport
                     vwAccountManagerCommission
                     OrderRpt
                     vwSalesFuelCommission
                     vwSalesFuelCommission_Old
                     vwSalesCommission
                     vwSalesCommission_OLD */
                    response.json({ success: true, message: 'Order and Probill Added Successfully. Order No:R0' + orderNo + '. Probill No:P0' + probillNo })
                }

                else
                    response.json({ success: false, message: 'Something went wrong. Record Not Inserted' })
            }
            else
                response.json({ success: false, message: 'Something went wrong. Record Not Inserted' })
        }
    } catch (error) {
        response.json({ success: false, message: 'Something went wrong. Record Not Inserted' })
    }

}

const updateLoadTender = async (req, response) => {
    try {
        var request = new sql.Request();
        var data = req.body;
        let payload = {
            orderNo: +data.orderNo,
            userId: -1,
            terms: data.terms,
            customerId: data.customerId,
            customerName: data.customerName,
            billToId: data.billToId,
            divisionId: data.divisionId,
            salesRepId: data.salesRepId,
            salesrepName: data.salesrepName,
            currency: data.currency,
            rateCurrency: data.rateCurrency,
            rateExchange: data.rateExchange,
            poNo: data.poNo,
            callersName: data.callersName,
            weightUnits: data.weightUnits,
            qtyUnits: data.qtyUnits,
            instructions: data.instructions,
            invoiceTotal: +data.invoiceTotal,
            waitingTime: +data.waitingTime,
            weight: data.weight,
            freightSummary: data.freightSummary,
            ordDistance: data.ordDistance,
            dispatchAlert: data.dispatchAlert,
            categoryId: data.categoryId,
            locationId: data.locationId,
            equipTypeId: data.equipTypeId,
            codCurrency: data.currency,
            hazMat: data.hazMat,
            truckload: data.truckload,
            farmOut: data.farmOut,
            shipperLocID: data.shipperLocID,
            pickupLocID: data.pickupLocID,
            pick_Scheduling: data.pick_Scheduling,
            pick_DT1: data.pick_DT1,
            pick_DT2: data.pick_DT2,
            pick_notes: data.pick_notes,
            consigneeLocId: data.consigneeLocId,
            deliveryLocID: data.deliveryLocID,
            del_Scheduling: data.del_Scheduling,
            del_DT1: data.del_DT1,
            del_DT2: data.del_DT2,
            del_notes: data.del_notes,
            customsBrokerId: data.customsBrokerId,
            puNumber: data.puNumber,
            productDesc: data.productDesc,
            delNumber: data.delNumber,
            chargeCode: data.chargeCode,
            chargeDesc: data.chargeDesc,
            chargeType: data.chargeType,
            moveDesc: data.moveDesc,
            pick_schedulingName: data.pick_schedulingName,
            del_schedulingName: data.del_schedulingName,
            pNotes: data.pick_notes == '' || data.pick_notes == null ? 0 : 1,
            dNotes: data.del_notes == '' || data.del_notes == null ? 0 : 1,
        }

        console.log('Order Already Exist')
        var prevDetails = await request.query("select o.orderno,p.probillno,p.shipperlocid,p.consigneelocid,convert(varchar, p.Pick_DT1, 21) Pick_DT1,convert(varchar, p.Pick_DT2, 21) Pick_DT2,convert(varchar, p.Del_DT1, 21) Del_DT1,convert(varchar, p.Del_DT2, 21)Del_DT2,p.Pick_Scheduling,p.Del_Scheduling,p.Pick_ReScheduling,p.Del_ReScheduling,p.PickupLocID,p.DeliveryLocID,o.invoicetotal from probill p join [order] o on p.orderno=o.orderno where o.pono='" + data.poNo + "'");
        var orderNo = prevDetails.recordset[0].orderno;
        var probillNo = prevDetails.recordset[0].probillno;
        var pick_DT1 = prevDetails.recordset[0].Pick_DT1;
        var pick_DT2 = prevDetails.recordset[0].Pick_DT2;
        var del_DT1 = prevDetails.recordset[0].Del_DT1;
        var del_DT2 = prevDetails.recordset[0].Del_DT2;
        var pick_Scheduling = prevDetails.recordset[0].Pick_Scheduling;
        var del_Scheduling = prevDetails.recordset[0].Del_Scheduling;
        var pick_ReScheduling = prevDetails.recordset[0].Pick_ReScheduling;
        var del_ReScheduling = prevDetails.recordset[0].Del_ReScheduling;
        var pickupLocID = prevDetails.recordset[0].PickupLocID;
        var deliveryLocID = prevDetails.recordset[0].DeliveryLocID;
        var invoiceTotal = prevDetails.recordset[0].invoicetotal;
        var shipperLocID = prevDetails.recordset[0].shipperlocid;
        var consigneeLocId = prevDetails.recordset[0].consigneelocid;

        if (pick_Scheduling != payload.pick_Scheduling || pick_ReScheduling != payload.pick_Scheduling) {
            if (payload.pick_schedulingName === 'Rescheduled Appointment' || payload.pick_schedulingName === 'Rescheduled Pickup Window' || payload.pick_schedulingName === 'Rescheduled Pickup On') {
                existingValues = await request.query("select  Pick_ReScheduling from probill where probillno=" + probillNo);
                existingValues = existingValues.recordset[0].Pick_ReScheduling ? existingValues.recordset[0].Pick_ReScheduling : 'NOTHING';
                if (existingValues !== 'NOTHING') {
                    var prevScheduledTypeName = await getPickupDeliveryScheduleTypesName('pickup', existingValues, true);
                }
                else
                    var prevScheduledTypeName = 'NOTHING';
                var insertOHQuery = "INSERT INTO [dbo].[OrderHistory](OrderNo,DateRecorded,RecordedBy,HistoryItem,NoteType,FieldName,probillNo)VALUES(" + orderNo + ",getDate()," + payload.userId + ",'THE PICKUP RE-ARRANGEMENT CHANGED FROM ''" + prevScheduledTypeName + "''" + " TO " + "''" + payload.pick_schedulingName + "''" + ".','Audit','PICKUP RE-ARRANGEMENT'," + probillNo + ");"
                await request.query(insertOHQuery);
                await request.query("update probill set Pick_ReScheduling='" + payload.pick_Scheduling + "' where probillno=" + probillNo);
            }
            else {
                existingValues = await request.query("select  Pick_Scheduling from probill where probillno=" + probillNo);
                existingValues = existingValues.recordset[0].Pick_Scheduling ? existingValues.recordset[0].Pick_Scheduling : 'NOTHING';
                if (existingValues !== 'NOTHING') {
                    var prevScheduledTypeName = await getPickupDeliveryScheduleTypesName('pickup', existingValues, false);
                }
                else
                    var prevScheduledTypeName = 'NOTHING';
                var instQ = "INSERT INTO [dbo].[OrderHistory](OrderNo,DateRecorded,RecordedBy,HistoryItem,NoteType,FieldName,probillNo)VALUES(" + orderNo + ",getDate()," + payload.userId + ",'THE PICKUP ARRANGEMENT CHANGED FROM ''" + prevScheduledTypeName + "''" + " TO " + "''" + payload.pick_schedulingName + "''" + ".','Audit','PICKUP ARRANGEMENT'," + probillNo + ");"
                await request.query(instQ);
                await request.query("update probill set Pick_Scheduling='" + payload.pick_Scheduling + "',Pick_ReScheduling=null where probillno=" + probillNo);
            }
        }
        if (del_Scheduling != payload.del_Scheduling || del_ReScheduling != payload.del_Scheduling) {
            if (payload.del_schedulingName === 'Rescheduled Appointment' || payload.del_schedulingName === 'Rescheduled Delivery Window' || payload.del_schedulingName === 'Rescheduled For No Later Than') {
                existingValues = await request.query("select  Del_ReScheduling from probill where probillno=" + probillNo);
                existingValues = existingValues.recordset[0].Del_ReScheduling ? existingValues.recordset[0].Del_ReScheduling : 'NOTHING';
                if (existingValues !== 'NOTHING') {
                    var prevScheduledTypeName = await getPickupDeliveryScheduleTypesName('delivery', existingValues, true);
                }
                else
                    var prevScheduledTypeName = 'NOTHING';
                var insertOHQuery = "INSERT INTO [dbo].[OrderHistory](OrderNo,DateRecorded,RecordedBy,HistoryItem,NoteType,FieldName,probillNo)VALUES(" + orderNo + ",getDate()," + payload.userId + ",'THE DELIVERY RE-ARRANGEMENT CHANGED FROM ''" + prevScheduledTypeName + "''" + " TO " + "''" + payload.del_schedulingName + "''" + ".','Audit','DELIVERY RE-ARRANGEMENT'," + probillNo + ");"
                await request.query(insertOHQuery);
                await request.query("update probill set del_ReScheduling='" + payload.del_Scheduling + "' where probillno=" + probillNo);
            }
            else {
                existingValues = await request.query("select  Del_Scheduling from probill where probillno=" + probillNo);
                existingValues = existingValues.recordset[0].Del_Scheduling ? existingValues.recordset[0].Del_Scheduling : 'NOTHING';
                if (existingValues !== 'NOTHING') {
                    var prevScheduledTypeName = await getPickupDeliveryScheduleTypesName('delivery', existingValues, false);
                }
                else
                    var prevScheduledTypeName = 'NOTHING';
                var insertOHQuery = "INSERT INTO [dbo].[OrderHistory](OrderNo,DateRecorded,RecordedBy,HistoryItem,NoteType,FieldName,probillNo)VALUES(" + orderNo + ",getDate()," + payload.userId + ",'THE DELIVERY ARRANGEMENT CHANGED FROM ''" + prevScheduledTypeName + "''" + " TO " + "''" + payload.del_schedulingName + "''" + ".','Audit','DELIVERY ARRANGEMENT'," + probillNo + ");"
                await request.query(insertOHQuery);
                await request.query("update probill set del_Scheduling='" + payload.del_Scheduling + "',Pick_ReScheduling=null where probillno=" + probillNo);
            }
        }

        if (pick_DT1 != payload.pick_DT1) {
            if (pick_ReScheduling) {
                await request.query("update probill set pick_rdt1='" + payload.pick_DT1 + "' where probillno=" + probillNo);
                pick_DT1 = pick_DT1 ? pick_DT1 : 'NOTHING'
                payload.pick_DT1 = payload.pick_DT1 ? payload.pick_DT1 : 'NOTHING'
                pick_DT1 = pick_DT1 != 'NOTHING' ? moment(pick_DT1).format('YYYY/MM/DD') + ' ' + moment(pick_DT1).format('LTS') : 'NOTHING';
                payload.pick_DT1 = payload.pick_DT1 != 'NOTHING' ? moment(payload.pick_DT1).format('YYYY/MM/DD') + ' ' + moment(payload.pick_DT1).format('LTS') : 'NOTHING';
                var insertOHQuery = "INSERT INTO [dbo].[OrderHistory](OrderNo,DateRecorded,RecordedBy,HistoryItem,NoteType,FieldName,probillNo)VALUES(" + orderNo + ",getDate()," + payload.userId + ",'THE RE-SCHEDULED PICKUP START TIME CHANGED FROM '" + "'" + pick_DT1 + "'" + "' TO '" + "'" + payload.pick_DT1 + "'" + "'.','Audit','RE-SCHEDULED PICKUP START TIME'," + probillNo + ");"
                await request.query(insertOHQuery);
            }
            else {
                await request.query("update probill set pick_dt1='" + payload.pick_DT1 + "',pick_date1='" + payload.pick_DT1 + "' where probillno=" + probillNo);
                pick_DT1 = pick_DT1 ? pick_DT1 : 'NOTHING'
                payload.pick_DT1 = payload.pick_DT1 ? payload.pick_DT1 : 'NOTHING';
                pick_DT1 = pick_DT1 != 'NOTHING' ? moment(pick_DT1).format('YYYY/MM/DD') + ' ' + moment(pick_DT1).format('LTS') : 'NOTHING';
                payload.pick_DT1 = payload.pick_DT1 != 'NOTHING' ? moment(payload.pick_DT1).format('YYYY/MM/DD') + ' ' + moment(payload.pick_DT1).format('LTS') : 'NOTHING';
                var insertOHQuery = "INSERT INTO [dbo].[OrderHistory](OrderNo,DateRecorded,RecordedBy,HistoryItem,NoteType,FieldName,probillNo)VALUES(" + orderNo + ",getDate()," + payload.userId + ",'THE SCHEDULED PICKUP START TIME CHANGED FROM '" + "'" + pick_DT1 + "'" + "' TO '" + "'" + payload.pick_DT1 + "'" + "'.','Audit','SCHEDULED PICKUP START TIME'," + probillNo + ");"
                await request.query(insertOHQuery);
            }
        }
        if (pick_DT2 != payload.pick_DT2) {
            if (pick_ReScheduling) {
                await request.query("update probill set pick_rdt2='" + payload.pick_DT2 + "' where probillno=" + probillNo);
                pick_DT2 = pick_DT2 ? pick_DT2 : 'NOTHING'
                payload.pick_DT2 = payload.pick_DT2 ? payload.pick_DT2 : 'NOTHING'
                pick_DT2 = pick_DT2 != 'NOTHING' ? moment(pick_DT2).format('YYYY/MM/DD') + ' ' + moment(pick_DT2).format('LTS') : 'NOTHING';
                payload.pick_DT2 = payload.pick_DT2 != 'NOTHING' ? moment(payload.pick_DT2).format('YYYY/MM/DD') + ' ' + moment(payload.pick_DT2).format('LTS') : 'NOTHING';
                var insertOHQuery = "INSERT INTO [dbo].[OrderHistory](OrderNo,DateRecorded,RecordedBy,HistoryItem,NoteType,FieldName,probillNo)VALUES(" + orderNo + ",getDate()," + payload.userId + ",'THE RE-SCHEDULED PICKUP END TIME CHANGED FROM '" + "'" + pick_DT2 + "'" + "' TO '" + "'" + payload.pick_DT2 + "'" + "'.','Audit','RE-SCHEDULED PICKUP END TIME'," + probillNo + ");"
                await request.query(insertOHQuery);
            }
            else {
                await request.query("update probill set pick_dt2='" + payload.pick_DT2 + "',pick_date2='" + payload.pick_DT2 + "' where probillno=" + probillNo);
                pick_DT2 = pick_DT2 ? pick_DT2 : 'NOTHING'
                payload.pick_DT2 = payload.pick_DT2 ? payload.pick_DT2 : 'NOTHING'
                pick_DT2 = pick_DT2 != 'NOTHING' ? moment(pick_DT2).format('YYYY/MM/DD') + ' ' + moment(pick_DT2).format('LTS') : 'NOTHING';
                payload.pick_DT2 = payload.pick_DT2 != 'NOTHING' ? moment(payload.pick_DT2).format('YYYY/MM/DD') + ' ' + moment(payload.pick_DT2).format('LTS') : 'NOTHING';
                var insertOHQuery = "INSERT INTO [dbo].[OrderHistory](OrderNo,DateRecorded,RecordedBy,HistoryItem,NoteType,FieldName,probillNo)VALUES(" + orderNo + ",getDate()," + payload.userId + ",'THE SCHEDULED PICKUP END TIME CHANGED FROM '" + "'" + pick_DT2 + "'" + "' TO '" + "'" + payload.pick_DT2 + "'" + "'.','Audit','SCHEDULED PICKUP END TIME'," + probillNo + ");"
                await request.query(insertOHQuery);
            }
        }
        if (del_DT1 != payload.del_DT1) {

            if (del_ReScheduling) {
                await request.query("update probill set del_rdt1='" + payload.del_DT1 + "' where probillno=" + probillNo);
                del_DT1 = del_DT1 ? del_DT1 : 'NOTHING'
                payload.del_DT1 = payload.del_DT1 ? payload.del_DT1 : 'NOTHING'
                del_DT1 = del_DT1 != 'NOTHING' ? moment(del_DT1).format('YYYY/MM/DD') + ' ' + moment(del_DT1).format('LTS') : 'NOTHING';
                payload.del_DT1 = payload.del_DT1 != 'NOTHING' ? moment(payload.del_DT1).format('YYYY/MM/DD') + ' ' + moment(payload.del_DT1).format('LTS') : 'NOTHING';
                var insertOHQuery = "INSERT INTO [dbo].[OrderHistory](OrderNo,DateRecorded,RecordedBy,HistoryItem,NoteType,FieldName,probillNo)VALUES(" + orderNo + ",getDate()," + payload.userId + ",'THE RE-SCHEDULED DELIVERY START TIME CHANGED FROM '" + "'" + del_DT1 + "'" + "' TO '" + "'" + payload.del_DT1 + "'" + "'.','Audit','RE-SCHEDULED DELIVERY START TIME'," + probillNo + ");"
                await request.query(insertOHQuery);
            }
            else {
                await request.query("update probill set del_dt1='" + payload.del_DT1 + "',del_date1='" + payload.del_DT1 + "' where probillno=" + probillNo);
                del_DT1 = del_DT1 ? del_DT1 : 'NOTHING'
                payload.del_DT1 = payload.del_DT1 ? payload.del_DT1 : 'NOTHING'
                del_DT1 = del_DT1 != 'NOTHING' ? moment(del_DT1).format('YYYY/MM/DD') + ' ' + moment(del_DT1).format('LTS') : 'NOTHING';
                payload.del_DT1 = payload.del_DT1 != 'NOTHING' ? moment(payload.del_DT1).format('YYYY/MM/DD') + ' ' + moment(payload.del_DT1).format('LTS') : 'NOTHING';
                var insertOHQuery = "INSERT INTO [dbo].[OrderHistory](OrderNo,DateRecorded,RecordedBy,HistoryItem,NoteType,FieldName,probillNo)VALUES(" + orderNo + ",getDate()," + payload.userId + ",'THE SCHEDULED DELIVERY START TIME CHANGED FROM '" + "'" + del_DT1 + "'" + "' TO '" + "'" + payload.del_DT1 + "'" + "'.','Audit','SCHEDULED DELIVERY START TIME'," + probillNo + ");"
                await request.query(insertOHQuery);
            }
        }
        if (del_DT2 != payload.del_DT2) {
            if (del_ReScheduling) {
                await request.query("update probill set del_rdt1='" + payload.del_DT1 + "' where probillno=" + probillNo);
                del_DT2 = del_DT2 ? del_DT2 : 'NOTHING'
                payload.del_DT2 = payload.del_DT2 ? payload.del_DT2 : 'NOTHING'
                del_DT2 = del_DT2 != 'NOTHING' ? moment(del_DT2).format('YYYY/MM/DD') + ' ' + moment(del_DT2).format('LTS') : 'NOTHING';
                payload.del_DT2 = payload.del_DT2 != 'NOTHING' ? moment(payload.del_DT2).format('YYYY/MM/DD') + ' ' + moment(payload.del_DT2).format('LTS') : 'NOTHING';
                var insertOHQuery = "INSERT INTO [dbo].[OrderHistory](OrderNo,DateRecorded,RecordedBy,HistoryItem,NoteType,FieldName,probillNo)VALUES(" + orderNo + ",getDate()," + payload.userId + ",'THE SCHEDULED DELIVERY END TIME CHANGED FROM '" + "'" + del_DT2 + "'" + "' TO '" + "'" + payload.del_DT2 + "'" + "'.','Audit','SCHEDULED DELIVERY END TIME'," + probillNo + ");"
                await request.query(insertOHQuery);
            }
            else {
                await request.query("update probill set del_dt1='" + payload.del_DT1 + "',del_date1='" + payload.del_DT1 + "' where probillno=" + probillNo);
                del_DT2 = del_DT2 ? del_DT2 : 'NOTHING'
                payload.del_DT2 = payload.del_DT2 ? payload.del_DT2 : 'NOTHING'
                del_DT2 = del_DT2 != 'NOTHING' ? moment(del_DT2).format('YYYY/MM/DD') + ' ' + moment(del_DT2).format('LTS') : 'NOTHING';
                payload.del_DT2 = payload.del_DT2 != 'NOTHING' ? moment(payload.del_DT2).format('YYYY/MM/DD') + ' ' + moment(payload.del_DT2).format('LTS') : 'NOTHING';
                var insertOHQuery = "INSERT INTO [dbo].[OrderHistory](OrderNo,DateRecorded,RecordedBy,HistoryItem,NoteType,FieldName,probillNo)VALUES(" + orderNo + ",getDate()," + payload.userId + ",'THE SCHEDULED DELIVERY END TIME CHANGED FROM '" + "'" + del_DT2 + "'" + "' TO '" + "'" + payload.del_DT2 + "'" + "'.','Audit','SCHEDULED DELIVERY END TIME'," + probillNo + ");"
                await request.query(insertOHQuery);
            }
        }
        if (shipperLocID != payload.shipperLocID) {
            var prevLocName = await request.query("select location from location where locid=" + shipperLocID);
            prevLocName = prevLocName.recordset[0].location;
            var currLocName = await request.query("select location from location where locid=" + payload.shipperLocID);
            currLocName = currLocName.recordset[0].location;
            var insertOHQuery = "INSERT INTO [dbo].[OrderHistory](OrderNo,DateRecorded,RecordedBy,HistoryItem,NoteType,FieldName,probillNo)VALUES(" + orderNo + ",getDate()," + payload.userId + ",'THE SHIPPER LOCATION CHANGED FROM ''" + prevLocName + "'' TO ''" + currLocName + "''','Audit','SHIPPER LOCATION'," + probillNo + ");"
            await request.query(insertOHQuery);
            await request.query("update probill set shipperlocid='" + payload.shipperLocID + "' where probillno=" + probillNo);
        }
        if (pickupLocID != payload.pickupLocID) {
            var prevLocName = await request.query("select location from location where locid=" + pickupLocID);
            prevLocName = prevLocName.recordset[0].location;
            var currLocName = await request.query("select location from location where locid=" + payload.pickupLocID);
            currLocName = currLocName.recordset[0].location;
            var insertOHQuery = "INSERT INTO [dbo].[OrderHistory](OrderNo,DateRecorded,RecordedBy,HistoryItem,NoteType,FieldName,probillNo)VALUES(" + orderNo + ",getDate()," + payload.userId + ",'THE PICKUP LOCATION CHANGED FROM ''" + prevLocName + "'' TO ''" + currLocName + "''','Audit','PICKUP LOCATION'," + probillNo + ");"
            await request.query(insertOHQuery);
            await request.query("update probill set locationid=" + payload.pickupLocID + ", pickuplocid=" + payload.pickupLocID + " where probillno=" + probillNo);
        }
        if (consigneeLocId !== payload.consigneeLocId) {
            var prevLocName = await request.query("select location from location where locid=" + consigneeLocId);
            prevLocName = prevLocName.recordset[0].location;
            var currLocName = await request.query("select location from location where locid=" + payload.consigneeLocId);
            currLocName = currLocName.recordset[0].location;
            var insertOHQuery = "INSERT INTO [dbo].[OrderHistory](OrderNo,DateRecorded,RecordedBy,HistoryItem,NoteType,FieldName,probillNo)VALUES(" + orderNo + ",getDate()," + payload.userId + ",'THE CONSIGNEE LOCATION CHANGED FROM ''" + prevLocName + "'' TO ''" + currLocName + "''','Audit','CONSIGNEE LOCATION'," + probillNo + ");"
            await request.query(insertOHQuery);
            await request.query("update probill set consigneeLocId='" + payload.consigneeLocId + "' where probillno=" + probillNo);
        }
        if (deliveryLocID != payload.deliveryLocID) {
            var prevLocName = await request.query("select location from location where locid=" + deliveryLocID);
            prevLocName = prevLocName.recordset[0].location;
            var currLocName = await request.query("select location from location where locid=" + payload.deliveryLocID);
            currLocName = currLocName.recordset[0].location;
            var insertOHQuery = "INSERT INTO [dbo].[OrderHistory](OrderNo,DateRecorded,RecordedBy,HistoryItem,NoteType,FieldName,probillNo)VALUES(" + orderNo + ",getDate()," + payload.userId + ",'THE DELIVERY LOCATION CHANGED FROM ''" + prevLocName + "'' TO ''" + currLocName + "''','Audit','DELIVERY LOCATION'," + probillNo + ");"
            await request.query(insertOHQuery);
            await request.query("update probill set deliveryLocID='" + payload.deliveryLocID + "' where probillno=" + probillNo);
        }
        if (invoiceTotal != payload.invoiceTotal) {
            var insertOHQuery = "INSERT INTO [dbo].[OrderHistory](OrderNo,DateRecorded,RecordedBy,HistoryItem,NoteType,FieldName,probillNo)VALUES(" + orderNo + ",getDate()," + payload.userId + ",'THE INVOICE TOTAL CHANGED FROM " + "'" + invoiceTotal + "'" + " TO '" + "'" + payload.invoiceTotal + "'" + ",'Audit','INVOICE TOTAL'," + probillNo + ");"
            await request.query(insertOHQuery);
            await request.query("update [order] set invoicetotal=" + payload.invoiceTotal + ",FSBase=" + payload.invoiceTotal + " where orderno=" + orderNo);
            await request.query("update probill set revenue=" + payload.invoiceTotal + ",revdriver=" + payload.invoiceTotal + ",revcommission=" + payload.invoiceTotal + " where probillno=" + probillNo);
            await request.query("update ordercharge set unitprice=" + payload.invoiceTotal + ",totalamount=" + payload.invoiceTotal + ",oldtotal=" + payload.invoiceTotal + ",oldunitprice=" + payload.invoiceTotal + " where orderno=" + orderNo);
        }
        response.json({ success: true, message: 'Order and Probill already exists. Order No:R0' + orderNo + '. Probill No:P0' + probillNo + '. Data updated as per new values' })

    } catch (error) {
        response.json({ success: false, message: 'Something went wrong. Record Not Inserted' })
    }

}


module.exports = {
    getCommentsOnTrip,
    paymentTerms,
    categories,
    customBrokers,
    pickupScheduleTypes,
    deliveryScheduleTypes,
    divisions,
    salesReps,
    currencies,
    billingCompany,
    equipmentTypes,
    insertLoadTender,
    locations,
    customer,
    units,
    accChargeTypes,
    updateLoadTender,
    checkOrderExists
}
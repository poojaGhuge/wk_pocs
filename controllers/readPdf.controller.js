const http = require('http')
const csv = require('csv-parser');
const sendPdf = function (req, res) {
    var url = '//whkgvirsrvfs01/FleetManager_Docs/Store/20200408_1/325509_AKASHDEEP BANSAL DL.pdf'; //pdf link
    res.sendFile(url, null, function (err) {
        console.log(err);
        res.end();
    });
}

const fs = require('fs');
const readCSV = (req, res) => {

    fs.createReadStream('//WHKGVIRSRVFS01/data/uAttend_Data/Punch_Report_09-21-2020_10-04-2020_10-04-2020_12PM.csv')
        .pipe(csv())
        .on('data', (row) => {
            console.log(row);
        })
        .on('end', () => {
            console.log('CSV file successfully processed');
        });
}


module.exports = {
    sendPdf,
    readCSV
}
const APIKEY = '9DF73205D028D446B9ABA4090EB44E5C';
const request = require('request-promise');
const GeoCoding = async (street, city, state, country) => {
    var countryCode;
    switch (country) {
        case 'MEXICO':
            countryCode = 'MX';
            break;
        case 'USA':
        case 'US':
            countryCode = 'US'
            break;
        case 'CANADA':
            countryCode = 'CA'
            break;
        default:
            countryCode = country;
    }
    var url = 'https://pcmiler.alk.com/apis/rest/v1.0/Service.svc/locations?'
    url += street ? url + 'street=' + street + '&' : ''
    url += 'city=' + city + ' & state=' + state + ' & country=' + countryCode + ' & dataset=Current';
    url = encodeURI(url);
    url = url.replace('#', '%23')
    const httpOptions = {
        url: url,
        method: 'GET',
        "headers": {
            "Content-type": "application/json",
            'Authorization': APIKEY
        }
    };
    return new Promise((resolve, reject) => {
        request(httpOptions, (error, response) => {
            if (error) {
                reject(error);
            } else if (response && response.statusCode === 200) {
                var data = JSON.parse(response.body);
                if (data[0] && data[0].Coords) {
                    resolve({ lat: data[0].Coords.Lat, lon: data[0].Coords.Lon });
                } else {
                    resolve({ lat: null, lon: null });
                }
            } else {
                resolve({ lat: null, lon: null });
            }
        });
    });

};
const GeoCodingSearchString = async (searchString) => {

    var url = 'https://pcmiler.alk.com/apis/rest/v1.0/Service.svc/locations?searchString=' + searchString + ' & dataset=Current';
    url = encodeURI(url);
    url = url.replace('#', '%23')
    const httpOptions = {
        url: url,
        method: 'GET',
        "headers": {
            "Content-type": "application/json",
            'Authorization': APIKEY
        }
    };
    return new Promise((resolve, reject) => {
        request(httpOptions, (error, response) => {
            if (error) {
                reject(error);
            } else if (response && response.statusCode === 200) {
                var data = JSON.parse(response.body);
                if (data[0] && data[0].Coords) {
                    resolve({ lat: data[0].Coords.Lat, lon: data[0].Coords.Lon });
                } else {
                    resolve({ lat: null, lon: null });
                }
            } else {
                resolve({ lat: null, lon: null });
            }
        });
    });

};

const PostalCodeSearch = async (country, postalCode) => {
    var url = 'https://pcmiler.alk.com/apis/rest/v1.0/Service.svc/locations?postcode=' + postalCode + ''
    if (country === 'U')
        url += '&postcodeFilter=US'
    if (country === 'M')
        url += '&postcodeFilter=Mexico'
    url += '&region=NA & dataset=Current';
    url = encodeURI(url);
    url = url.replace('#', '%23')
    const httpOptions = {
        url: url,
        method: 'GET',
        "headers": {
            "Content-type": "application/json",
            'Authorization': APIKEY
        }
    };
    return new Promise((resolve, reject) => {
        request(httpOptions, (error, response) => {
            if (error) {
                reject(error);
            } else if (response && response.statusCode === 200) {
                var data = JSON.parse(response.body);
                if (data[0] && data[0].Coords) {
                    resolve({ lat: data[0].Coords.Lat, lon: data[0].Coords.Lon });
                } else {
                    resolve({ lat: null, lon: null });
                }
            } else {
                resolve({ lat: null, lon: null });
            }
        });
    });

};


const ReverseGeoCoding = async (lat, lon) => {
    var url = 'https://pcmiler.alk.com/apis/rest/v1.0/Service.svc/locations/reverse?Coords=' + lon + ',' + lat + '&matchNamedRoadsOnly=true&maxCleanupMiles=20&dataset=Current';
    url = encodeURI(url);
    url = url.replace('#', '%23')
    const httpOptions = {
        url: url,
        method: 'GET',
        "headers": {
            "Content-type": "application/json",
            'Authorization': APIKEY
        }
    };
    return new Promise((resolve, reject) => {
        request(httpOptions, (error, response) => {
            if (error) {
                reject(error);
            } else if (response && response.statusCode === 200) {
                var data = JSON.parse(response.body);
                if (data.Address) {
                    const address = data.Address.StreetAddress
                    const city = data.Address.City
                    const stateCode = data.Address.StateAbbreviation
                    const countryCode = data.Address.CountryAbbreviation
                    const timeZone = data.TimeZone
                    resolve({
                        address: address,
                        city: city,
                        stateCode: stateCode,
                        countryCode: countryCode,
                        timeZone: timeZone
                    });
                } else {
                    resolve({
                        address: null,
                        city: null,
                        stateCode: null,
                        countryCode: null,
                        timeZone: null
                    });
                }
            } else {
                resolve({
                    address: null,
                    city: null,
                    stateCode: null,
                    countryCode: null,
                    timeZone: null
                });
            }
        });
    });

};


const CalculateMiles = async (oLonLat, dLonLat) => {
    try {
        const url = 'https://pcmiler.alk.com/apis/rest/v1.0/Service.svc/route/routeReports?stops=' + oLonLat + ';' + dLonLat + '&vehType=Truck&&reports=CalcMiles';
        const httpOptions = {
            url: url,
            method: 'GET',
            "headers": {
                "Content-type": "application/json",
                'Authorization': APIKEY
            }
        };
        return new Promise((resolve, reject) => {
            request(httpOptions, (error, response) => {
                if (error) {
                    reject(error);
                } else if (response && response.statusCode === 200) {
                    var data = JSON.parse(response.body);
                    if (data[0]) {
                        var miles = data[0].TMiles
                        resolve(miles);
                    } else {
                        resolve(null);
                    }
                } else {
                    resolve(null);
                }
            });
        });
    } catch (error) {
        return ({ miles: '-' })
    }
};
const GeoCodingBatch = async (data) => {
    try {
        const url = 'https://pcmiler.alk.com/apis/rest/v1.0/service.svc/locations/address/batch';
        const httpOptions = {
            url: url,
            method: 'POST',
            "headers": {
                "Content-type": "application/json",
                'Authorization': APIKEY
            },
            json: true,
            body: {
                "Locations": data
            }

        };
        return new Promise((resolve, reject) => {
            request(httpOptions, (error, response) => {
                if (error) {
                    reject(error);
                } else if (response && response.statusCode === 200) {
                    var data = response.body;
                    if (data) {
                        var result = [];
                        data.forEach(element => {
                            if (element.Errors.length === 0)
                                result.push({
                                    zip: element.Address.Zip.replace(/\s/g, ''),
                                    city: element.Address.City,
                                    lonlat: element.Coords.Lon + ',' + element.Coords.Lat,
                                });
                        });
                        resolve(result);
                    } else {
                        resolve([]);
                    }
                } else {
                    resolve(null);
                }
            });
        });
    } catch (error) {
        return ({ miles: '-' })
    }
};


const SearchNearbyPostalCodes = async (lonLat, radius) => {
    try {
        const url = 'https://pcmiler.alk.com/apis/rest/v1.0/service.svc/poi?center=' + lonLat + '&radius=' + radius + '&region=NA&radiusUnits=Miles&poiCategories=2&dataset=Current';
        const httpOptions = {
            url: url,
            method: 'GET',
            "headers": {
                "Content-type": "application/json",
                'Authorization': APIKEY
            }
        };
        return new Promise((resolve, reject) => {
            request(httpOptions, (error, response) => {
                if (error) {
                    reject(error);
                } else if (response && response.statusCode === 200) {
                    var data = JSON.parse(response.body);
                    resolve(data);
                } else {
                    resolve([]);
                }
            });
        });
    } catch (error) {
        return ({ miles: '-' })
    }
};


module.exports = {
    GeoCoding,
    CalculateMiles,
    ReverseGeoCoding,
    GeoCodingSearchString,
    GeoCodingBatch,
    SearchNearbyPostalCodes,
    PostalCodeSearch
}
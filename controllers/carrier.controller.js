var sql = require("mssql");

const InsertCarrier = async (req, res) => {
    try {
        var { carrierName, address1, address2, city, province, postalCode, phone, phoneExt, fax, telePrefix, tollFree, cellular, eMailAddress, WWWAddress, contact, contactPosition, iCCNumber, apCodeUS, aPCodeCDN, insCompany, insBroker, insContact, insPhone, insPhoneExt, insFax, insEMailAddress, insPolicyNo, insExpiryDate, insCargoAmount, insLiabilityAmount, notes, csa, csaNo, fast, fastNo, categoryID, carrierCode, dotNumber, cbsaCarrierCode, nirNumber } = req.body;
        var query = "insert into Carrier(carrierName,address1,address2,city,province,postalCode,phone,phoneExt,fax,telePrefix,tollFree,cellular,eMailAddress,WWWAddress,contact,contactPosition,iCCNumber, aPCodeCDN, aPCodeUS,insCompany,insBroker,insContact,insPhone,insPhoneExt,insFax,insEMailAddress,insPolicyNo,insExpiryDate,insCargoAmount,insLiabilityAmount,notes,csa,csaNo,fast,fastNo,status,categoryID,createdBy,CreationDate,carrierCode,dotNumber,cbsaCarrierCode,nirNumber) values ('" + carrierName + "', '" + address1 + "', '" + address2 + "', '" + city + "', '" + province + "', '" + postalCode + "', '" + phone + "', '" + phoneExt + "', '" + fax + "', '" + telePrefix + "', '" + tollFree + "', '" + cellular + "', '" + eMailAddress + "', '" + WWWAddress + "', '" + contact + "', '" + contactPosition + "', '" + iCCNumber + "', '" + aPCodeCDN + "', '" + apCodeUS + "', '" + insCompany + "', '" + insBroker + "', '" + insContact + "', '" + insPhone + "', '" + insPhoneExt + "', '" + insFax + "', '" + insEMailAddress + "', '" + insPolicyNo + "', '" + insExpiryDate + "', '" + insCargoAmount + "', '" + insLiabilityAmount + "', '" + notes + "', " + csa + ", '" + csaNo + "', " + fast + ", '" + fastNo + "', 'Active', " + categoryID + ",-1 ,getDate(),'" + carrierCode + "', '" + dotNumber + "', '" + cbsaCarrierCode + "', '" + nirNumber + "')";
        var request = new sql.Request();
        request.query(query, function (err, result) {
            if (err) {
                res.json({ success: false, err: err.message, message: 'Record not Inserted' })
            }
            else {
                console.log(result);
                res.json({ success: true, message: 'Record Inserted Successfully' })
            }
        });
    } catch (error) {
        res.json({ success: false, err: error.message, message: 'Record not Inserted' })
    }
}

const Categories = (req, res) => {
    try {
        var request = new sql.Request();
        request.query("select poolId,poolName from TemplatePool where PoolType='Carrier' and Status='Active'", function (err, recordset) {
            if (err) {
                res.json({ success: false, data: 'Records not found' })
            } else {
                res.json({ success: true, data: recordset.recordset })
            }
        });
    } catch (error) {
        res.json({ success: false, message: error.message })
    }
}

const Carriers = (req, res) => {
    try {
        var request = new sql.Request();
        request.query("select carrierName, address1, address2, city, province, postalCode, phone, phoneExt, fax, telePrefix, tollFree, cellular, eMailAddress, WWWAddress, contact, contactPosition, iCCNumber, apCodeUS, aPCodeCDN, insCompany, insBroker, insContact, insPhone, insPhoneExt, insFax, insEMailAddress, insPolicyNo, insExpiryDate, insCargoAmount, insLiabilityAmount, notes, csa, csaNo, fast, fastNo, c.status,categoryID,poolname categoryName, carrierCode, dotNumber, cbsaCarrierCode, nirNumber from carrier c join templatepool tp on tp.poolid=categoryid", function (err, recordset) {
            if (err) {
                res.json({ success: false, data: 'Records not found' })
            } else {
                res.json({ success: true, data: recordset.recordset })
            }
        });
    } catch (error) {
        res.json({ success: false, message: error.message })
    }
}

const UpdateCarrier = (req, res) => {
    try {
        var { carrierId, carrierName, address1, address2, city, province, postalCode, phone, phoneExt, fax, telePrefix, tollFree, cellular, eMailAddress, WWWAddress, contact, contactPosition, iCCNumber, apCodeUS, aPCodeCDN, insCompany, insBroker, insContact, insPhone, insPhoneExt, insFax, insEMailAddress, insPolicyNo, insExpiryDate, insCargoAmount, insLiabilityAmount, notes, csa, csaNo, fast, fastNo, categoryID, carrierCode, dotNumber, cbsaCarrierCode, nirNumber, status } = req.body;
        var request = new sql.Request();
        var query = "update carrier set  carrierName='" + carrierName + "'"
        query += address1 ? " ,address1='" + address1 + "' " : ""
        query += address2 ? " ,address2='" + address2 + "' " : ""
        query += city ? " ,city='" + city + "' " : ""
        query += province ? " ,province='" + province + "' " : ""
        query += postalCode ? " ,postalCode='" + postalCode + "' " : ""
        query += phone ? " ,phone='" + phone + "' " : ""
        query += phoneExt ? " ,phoneExt='" + phoneExt + "' " : ""
        query += fax ? " ,fax='" + fax + "' " : ""
        query += telePrefix ? " ,telePrefix='" + telePrefix + "' " : ""
        query += tollFree ? " ,tollFree='" + tollFree + "' " : ""
        query += cellular ? " ,cellular='" + cellular + "' " : ""
        query += eMailAddress ? " ,eMailAddress='" + eMailAddress + "' " : ""
        query += WWWAddress ? " ,WWWAddress='" + WWWAddress + "' " : ""
        query += contact ? " ,contact='" + contact + "' " : ""
        query += contactPosition ? " ,contactPosition='" + contactPosition + "' " : ""
        query += iCCNumber ? " ,iCCNumber='" + iCCNumber + "' " : ""
        query += apCodeUS ? " ,apCodeUS='" + apCodeUS + "' " : ""
        query += aPCodeCDN ? " ,aPCodeCDN='" + aPCodeCDN + "' " : ""
        query += insCompany ? " ,insCompany='" + insCompany + "' " : ""
        query += insBroker ? " ,insBroker='" + insBroker + "' " : ""
        query += insContact ? " ,insContact='" + insContact + "' " : ""
        query += insPhone ? " ,insPhone='" + insPhone + "' " : ""
        query += insPhoneExt ? " ,insPhoneExt='" + insPhoneExt + "' " : ""
        query += insFax ? " ,insFax='" + insFax + "' " : ""
        query += insEMailAddress ? " ,insEMailAddress='" + insEMailAddress + "' " : ""
        query += insPolicyNo ? " ,insPolicyNo='" + insPolicyNo + "' " : ""
        query += insExpiryDate ? " ,insExpiryDate='" + insExpiryDate + "' " : ""
        query += insCargoAmount ? " ,insCargoAmount='" + insCargoAmount + "' " : ""
        query += insLiabilityAmount ? " ,insLiabilityAmount='" + insLiabilityAmount + "' " : ""
        query += notes ? " ,notes='" + notes + "' " : ""
        query += csa ? " ,csa=" + csa + "" : ""
        query += csaNo ? " ,csaNo='" + csaNo + "' " : " ,csaNo='' "
        query += fast ? " ,fast=" + fast + " " : ""
        query += fastNo ? " ,fastNo='" + fastNo + "' " : " ,fastNo='' "
        query += categoryID ? " ,categoryID='" + categoryID + "' " : ""
        query += carrierCode ? " ,carrierCode='" + carrierCode + "' " : ""
        query += dotNumber ? " ,dotNumber='" + dotNumber + "' " : ""
        query += cbsaCarrierCode ? " ,cbsaCarrierCode='" + cbsaCarrierCode + "' " : ""
        query += nirNumber ? " ,nirNumber='" + nirNumber + "' " : ""
        query += status ? " ,status='" + status + "' " : ""

        query += "where carrierId=" + carrierId;
        request.query(query, function (err, recordset) {
            if (err) {
                res.json({ success: false, err: err.message, message: 'Record not updated' })
            }
            else {
                res.json({ success: true, message: 'Record updated Successfully' })
            }
        });
    } catch (error) {
        res.json({ success: false, message: error.message })
    }
}



module.exports = {
    Categories,
    InsertCarrier,
    Carriers,
    UpdateCarrier
}
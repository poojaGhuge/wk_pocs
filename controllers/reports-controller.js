var sql = require("mssql");
var _ = require('underscore');
const db = require('../db-conn');
var moment = require('moment');
var NodeGeocoder = require('node-geocoder');

var dbConfigForReports = {
    user: 'tpconnect',
    password: 'SecuRead9267#',
    server: '192.168.10.222\\SQLEXPRESS',
    database: 'FleetTraining',
    connectionTimeout: 999999999,
    requestTimeout: 999999999
};

const getCommentsOnTrip = (request, response) => {
    var request = new sql.Request();
    request.query(`
            SELECT RealName,[FleetTraining].[dbo].[Trip].TripID,[FleetTraining].[dbo].[Trip].Comment
  FROM [FleetTraining].[dbo].[User] inner join [FleetTraining].[dbo].[Trip] on 
  [FleetTraining].[dbo].[Trip].CreatedBy=[FleetTraining].[dbo].[User].UserID
  where len([FleetTraining].[dbo].[Trip].Comment) !=0 order by TripID desc
            `, function (err, recordset) {
        if (err) {
            console.log('error in fetching data ', err)
            response.json({ success: false, data: 'Records not found' })
        } else {
            response.json({ success: true, data: recordset })
        }
    });
}

module.exports = {
    getCommentsOnTrip,
}

var sql = require("mssql");

const Provinces = (req, res) => {
    try {
        var searchTerm = req.params.searchTerm
        var request = new sql.Request();
        request.query(`select jurId,name,code from jurisdiction where name like '%${searchTerm}%'`, function (err, recordset) {
            if (err) {
                res.json({ success: false, data: 'Records not found' })
            } else {
                res.json({ success: true, data: recordset.recordset })
            }
        });
    } catch (error) {
        res.json({ success: false, message: error.message })
    }

}

const ProvinceById = (req, res) => {
    try {
        var jurId = req.params.jurId
        var request = new sql.Request();
        request.query(`select jurId,name,code from jurisdiction where jurid = ${jurId}`, function (err, recordset) {
            if (err) {
                res.json({ success: false, data: 'Records not found' })
            } else {
                res.json({ success: true, data: recordset.recordset })
            }
        });
    } catch (error) {
        res.json({ success: false, message: error.message })
    }

}
const Divisions = (req, res) => {
    try {
        var request = new sql.Request();
        request.query(`select divisionId, divisionName from Division where status='Active'`, function (err, recordset) {
            if (err) {
                res.json({ success: false, data: 'Records not found' })
            } else {
                res.json({ success: true, data: recordset.recordset })
            }
        });
    } catch (error) {
        res.json({ success: false, message: error.message })
    }

}
const Drivers = (req, res) => {
    try {
        var request = new sql.Request();
        request.query(`select * from driver where status='Active'`, function (err, recordset) {
            if (err) {
                res.json({ success: false, data: 'Records not found' })
            } else {
                res.json({ success: true, data: recordset.recordset })
            }
        });
    } catch (error) {
        res.json({ success: false, message: error.message })
    }

}
const Driver = (req, res) => {
    try {
        var driverId = req.params.driverId
        var request = new sql.Request();
        request.query(`select * from driver where driverId=${driverId}`, function (err, recordset) {
            if (err) {
                res.json({ success: false, data: 'Records not found' })
            } else {
                res.json({ success: true, data: recordset.recordset })
            }
        });
    } catch (error) {
        res.json({ success: false, message: error.message })
    }

}
const Terminals = (req, res) => {
    try {
        var request = new sql.Request();
        request.query(`select terminalId,terminalName from Terminal`, function (err, recordset) {
            if (err) {
                res.json({ success: false, data: 'Records not found' })
            } else {
                res.json({ success: true, data: recordset.recordset })
            }
        });
    } catch (error) {
        res.json({ success: false, message: error.message })
    }

}
const Categories = (req, res) => {
    try {
        var request = new sql.Request();
        request.query(`select poolId,PoolName from TemplatePool where poolType='Driver' and status='Active'`, function (err, recordset) {
            if (err) {
                res.json({ success: false, data: 'Records not found' })
            } else {
                res.json({ success: true, data: recordset.recordset })
            }
        });
    } catch (error) {
        res.json({ success: false, message: error.message })
    }
}
const Countries = (req, res) => {
    try {
        var searchTerm = req.params.searchTerm
        var request = new sql.Request();
        var query = `select id,code,name from Country where name like '%${searchTerm}%'`
        request.query(query, function (err, recordset) {
            if (err) {
                res.json({ success: false, data: 'Records not found' })
            } else {
                res.json({ success: true, data: recordset.recordset })
            }
        });
    } catch (error) {
        res.json({ success: false, message: error.message })
    }
}
const CountryByCode = (req, res) => {
    try {
        var countryCode = req.params.countryCode
        var request = new sql.Request();
        var query = `select id,code,name from Country where code = '${countryCode}'`
        request.query(query, function (err, recordset) {
            if (err) {
                res.json({ success: false, data: 'Records not found' })
            } else {
                res.json({ success: true, data: recordset.recordset })
            }
        });
    } catch (error) {
        res.json({ success: false, message: error.message })
    }
}
const Classes = (req, res) => {
    try {
        var request = new sql.Request();
        request.query(`select distinct class from driver`, function (err, recordset) {
            if (err) {
                res.json({ success: false, data: 'Records not found' })
            } else {
                res.json({ success: true, data: recordset.recordset })
            }
        });
    } catch (error) {
        res.json({ success: false, message: error.message })
    }
}
const Roles = (req, res) => {
    try {
        const data = ['Any', 'Highway', 'Local', 'Linehaul']
        res.json({ success: true, data: data })
    } catch (error) {
        res.json({ success: false, message: error.message })
    }
}
const SatelliteProviders = (req, res) => {
    try {
        const data = ['OMNITRACS']
        res.json({ success: true, data: data })
    } catch (error) {
        res.json({ success: false, message: error.message })
    }
}
const TravelDocuments = (req, res) => {
    try {
        const data = ['Passport Number', 'Enhanced Drivers License', 'SENTRY Card', 'NEXUS Card', 'Visa (Non-Immigrant)', 'Visa (Immigrant)', 'Laser Visa (Border Crossing Card)', 'Permanent Residant Card(C1)', 'Permanent Resident Card(C2)', 'U.S. Alien Registration Card(A1)', 'U.S. Alien Registration Card(A2)', 'U.S.Passport Card', 'DHS Refugee Travel Document', 'DHS Re-entry Permit', 'Enhanced Tribal Card / INAC', 'U.S. Military ID Document', 'U.S. Merchant Mariner Document', 'Citizenship Document', 'Certificate of Indian Status Card', 'Certificate of Naturalization']
        res.json({ success: true, data: data })
    } catch (error) {
        res.json({ success: false, message: error.message })
    }
}
const ComplianceGroups = (req, res) => {
    try {
        var request = new sql.Request();
        request.query(`select groupId,groupName from compliancygroup where resourcename='Driver'`, function (err, recordset) {
            if (err) {
                res.json({ success: false, data: 'Records not found' })
            } else {
                res.json({ success: true, data: recordset.recordset })
            }
        });
    } catch (error) {
        res.json({ success: false, message: error.message })
    }
}
const ComplianceGroupItems = (req, res) => {
    try {
        var driverId = +req.params.driverId
        var request = new sql.Request();
        request.query("select GroupItemID,ItemName,ItemDateReq,ItemDescReq from CompliancyGroupItem where GroupID=(select groupid from compliancygroupresource where driverresourceid = " + driverId + ")", function (err, recordset) {
            if (err) {
                res.json({ success: false, data: 'Records not found' })
            } else {
                res.json({ success: true, data: recordset.recordset })
            }
        });
    } catch (error) {
        res.json({ success: false, message: error.message })
    }
}
const DriverCompliancy = (req, res) => {
    try {
        var { driverId } = req.params
        var request = new sql.Request();
        request.query("select dc.Item,dc.Description,dc.ExpiryDate,cg.ItemDateReq,cg.ItemDescReq from DriverCompliancy dc right join CompliancyGroupItem cg on cg.DriverItemName=dc.Item where DriverID=" + driverId + " and GroupID=(select groupid from CompliancyGroupResource where DriverResourceID=" + driverId + ")", function (err, recordset) {
            if (err) {
                res.json({ success: false, data: 'Records not found' })
            } else {
                res.json({ success: true, data: recordset.recordset })
            }
        });
    } catch (error) {
        res.json({ success: false, message: error.message })
    }
}
const DriverCompliancyGroup = (req, res) => {
    try {
        var { driverId } = req.params
        var request = new sql.Request();
        request.query("select groupid from CompliancyGroupResource where DriverResourceID=" + driverId, function (err, recordset) {
            if (err) {
                res.json({ success: false, data: 'Records not found' })
            } else {
                res.json({ success: true, data: recordset.recordset })
            }
        });
    } catch (error) {
        res.json({ success: false, message: error.message })
    }
}
const InsertComplianceItems = async (req, res) => {
    try {
        var { groupId, driverId } = req.body;
        var request = new sql.Request();
        var checkExists = await request.query("select groupid from compliancygroupresource where driverresourceid=" + driverId);
        if (checkExists.recordset[0] && checkExists.recordset[0].groupid)
            var query = "update compliancygroupresource set groupid=" + groupId + " where resourceid=" + driverId + " and driverresourceid=" + driverId + "";
        else
            var query = "insert into compliancygroupresource(groupid,resourceid,driverresourceid) values(" + groupId + "," + driverId + "," + driverId + ")";
        var request = new sql.Request();
        request.query(query, async function (err, result) {
            if (err) {
                res.json({ success: false, err: err.message, message: 'Record not Updated' })
            }
            else {
                var complianceItems = await request.query("select driverItemName from CompliancyGroupItem where groupId=" + groupId);
                var tempCom = complianceItems.recordset.map(d => `'${d.driverItemName}'`).join(',');
                var query = "delete from DriverCompliancy where driverId=" + driverId + " and item not in (" + tempCom + ")"
                var driverCompliancy = await request.query(query);
                var driverCompliancy = await request.query("select item from DriverCompliancy where driverId= " + driverId);
                var tempCompl = driverCompliancy.recordset.map(d => `'${d.item}'`).join(',');
                var newCompliancy = await request.query("select driverItemName from CompliancyGroupItem where groupId=" + groupId + " and driverItemName not in (" + tempCompl + ")")
                newCompliancy = newCompliancy.recordset;
                for (const _ of newCompliancy) {
                    await request.query("insert into DriverCompliancy(driverId,item,description) values(" + driverId + ",'" + _.driverItemName + "','')");
                }
                res.json({ success: true, message: 'Record Updated Successfully' })
            }
        });
    } catch (error) {
        res.json({ success: false, err: error.message, message: 'Record not Inserted' })
    }
}
const UpdateDriverCompliance = async (req, res) => {
    try {
        var { driverId, item, description, expiryDate } = req.body;
        var request = new sql.Request();
        var query

        query = description ? "update drivercompliancy set description = '" + description + "' where driverid=" + driverId + " and item='" + item + "';" : '';

        query += expiryDate ? "update drivercompliancy set expirydate='" + expiryDate + "' where driverid=" + driverId + " and item='" + item + "';" : '';
        var request = new sql.Request();
        request.query(query, function (err, result) {
            if (err) {
                res.json({ success: false, err: err.message, message: 'Record not Updated' })
            }
            else {
                res.json({ success: true, message: 'Record Updated Successfully' })
            }
        });
    } catch (error) {
        res.json({ success: false, err: error.message, message: 'Record not Inserted' })
    }
}

const InsertDriver = async (req, res) => {
    try {
        var { drvCode, firstName, lastName, address1, address2, city, jurId, postalCode, phone, fax, mobileNo, pager, email, socialInsNo, dob, startDate, role, classs, divisionId, terminalId, categoryId, fastNo, isFast, fastExpiryDate, gender, nationality, licenseNo, licenseProvince, licenseCountry, travelDocument, travelNo, travelCountry, licenseExpiryDate, travelNoExpiryDate, licenseType, satelliteProvider, satelliteProviderIdentifier, csa, csaNo, csaExpiryDate, notepad, employeeNo, federalTaxNumber, drivesFor } = req.body;
        csa = !csa ? 0 : csa
        var query = "INSERT INTO [dbo].[Driver](DrvCode,FirstName,LastName,Status,LocationID,Address1,Address2,City,Jurisdiction,PostalCode,Phone,Fax,Cellular,Pager,EMailAddress,SocialInsNo,BirthDate,StartDate,Role,Class,DivisionID,TerminalID,CategoryID,FASTNo,FAST,CreatedBy,CreationDate,CurrentStatus,Gender,Nationality,LicenseNo,LicenseProv,LicenseCountry,TravelDocument,TravelNo,TravelCountry,LicenseNoExpiryDate,TravelNoExpiryDate,LicenseType,SatelliteProvider,SatelliteProviderTrackingIdentifier, csa, csaNo,notepad)VALUES('" + drvCode + "','" + firstName + "','" + lastName + "','ACTIVE',(select terminallocid from terminal where terminalId=" + terminalId + "),'" + address1 + "','" + address2 + "','" + city + "','" + jurId + "','" + postalCode + "','" + phone + "','" + fax + "','" + mobileNo + "','" + pager + "','" + email + "','" + socialInsNo + "','" + dob + "','" + startDate + "','" + role + "','" + classs + "'," + divisionId + "," + terminalId + "," + categoryId + ",'" + fastNo + "'," + isFast + ",-1,getdate(),'AVAILABLE','" + gender + "','" + nationality + "','" + licenseNo + "','" + licenseProvince + "','" + licenseCountry + "','" + travelDocument + "','" + travelNo + "','" + travelCountry + "','" + licenseExpiryDate + "','" + travelNoExpiryDate + "','" + licenseType + "','" + satelliteProvider + "','" + satelliteProviderIdentifier + "', " + csa + ", '" + csaNo + "', '" + notepad + "') ";
        var request = new sql.Request();
        request.query(query, async function (err, result) {
            if (err) {
                res.json({ success: false, err: err.message, message: 'Record not Inserted' })
            }
            else {
                if (csaExpiryDate)
                    await request.query("update driver set csaExpiryDate='" + csaExpiryDate + "' where drvcode='" + drvCode + "'")
                if (fastExpiryDate)
                    await request.query("update driver set fastExpiryDate='" + fastExpiryDate + "' where drvcode='" + drvCode + "'")
                if (employeeNo)
                    await request.query("update driver set employeeNo='" + employeeNo + "' where drvcode='" + drvCode + "'")
                if (federalTaxNumber)
                    await request.query("update driver set federalTaxNumber='" + federalTaxNumber + "' where drvcode='" + drvCode + "'")
                if (drivesFor)
                    await request.query("update driver set drivesFor='" + drivesFor + "' where drvcode='" + drvCode + "'")
                console.log(result);
                res.json({ success: true, message: 'Record Inserted Successfully' })
            }
        });
    } catch (error) {
        res.json({ success: false, err: error.message, message: 'Record not Inserted' })
    }
}
const UpdateDriver = async (req, res) => {
    try {
        var { driverId, firstName, lastName, address1, address2, city, jurId, postalCode, phone, fax, mobileNo, pager, email, socialInsNo, dob, startDate, role, classs, divisionId, terminalId, categoryId, fastNo, isFast, fastExpiryDate, gender, nationality, licenseNo, licenseProvince, licenseCountry, travelDocument, travelNo, travelCountry, licenseExpiryDate, travelNoExpiryDate, licenseType, satelliteProvider, satelliteProviderIdentifier, csa, csaNo, csaExpiryDate, notepad, employeeNo, federalTaxNumber } = req.body;
        var query = "update Driver set  FirstName='" + firstName + "'"

        query += lastName ? " ,LastName='" + lastName + "'" : ''
        query += address1 ? " ,Address1='" + address1 + "'" : ''
        query += address2 ? " ,Address2='" + address2 + "'" : ''
        query += city ? " ,City='" + city + "'" : ''
        query += jurId ? " ,Jurisdiction='" + jurId + "'" : ''
        query += postalCode ? " ,PostalCode='" + postalCode + "'" : ''
        query += phone ? " ,Phone='" + phone + "'" : ''
        query += fax ? " ,Fax='" + fax + "'" : ''
        query += mobileNo ? " ,Cellular='" + mobileNo + "'" : ''
        query += pager ? " ,Pager='" + pager + "'" : ''
        query += email ? " ,EMailAddress='" + email + "'" : ''
        query += socialInsNo ? " ,SocialInsNo='" + socialInsNo + "'" : ''
        query += dob ? " ,BirthDate='" + dob + "'" : ''
        query += startDate ? " ,StartDate='" + startDate + "'" : ''
        query += role ? "  ,Role='" + role + "'" : ''
        query += classs ? " ,Class='" + classs + "'" : ''
        query += divisionId ? " ,DivisionID=" + divisionId + "" : ''
        query += terminalId ? " ,LocationID=(select terminallocid from terminal where terminalId=" + terminalId + "),TerminalID=" + terminalId + "" : ''
        query += categoryId ? " ,CategoryID=" + categoryId + "" : ''
        query += fastNo ? " ,FASTNo='" + fastNo + "'" : ''
        query += isFast ? " ,FAST=" + isFast + "" : ''
        query += fastExpiryDate ? " ,fastExpiryDate='" + fastExpiryDate + "'" : ''
        query += gender ? " ,Gender='" + gender + "'" : ''
        query += nationality ? " ,Nationality='" + nationality + "'" : ''
        query += licenseNo ? " ,LicenseNo='" + licenseNo + "'" : ''
        query += licenseProvince ? " ,LicenseProv='" + licenseProvince + "'" : ''
        query += licenseCountry ? " ,LicenseCountry='" + licenseCountry + "'" : ''
        query += travelDocument ? " ,TravelDocument='" + travelDocument + "'" : ''
        query += travelNo ? " ,TravelNo='" + travelNo + "'" : ''
        query += travelCountry ? " ,TravelCountry='" + travelCountry + "'" : ''
        query += licenseExpiryDate ? " ,LicenseNoExpiryDate='" + licenseExpiryDate + "'" : ''
        query += travelNoExpiryDate ? " ,TravelNoExpiryDate='" + travelNoExpiryDate + "'" : ''
        query += licenseType ? " ,LicenseType='" + licenseType + "'" : ''
        query += satelliteProvider ? " ,SatelliteProvider='" + satelliteProvider + "'" : ''
        query += satelliteProviderIdentifier ? " ,SatelliteProviderTrackingIdentifier='" + satelliteProviderIdentifier + "'" : ''
        query += csa ? " ,csa=" + csa + "" : ''
        query += csaNo ? " ,csaNo='" + csaNo + "'" : ''
        query += csaExpiryDate ? " ,csaExpiryDate='" + csaExpiryDate + "'" : ''
        query += notepad ? " ,notepad='" + notepad + "'" : ''
        query += employeeNo ? " ,employeeNo='" + employeeNo + "'" : ''
        query += federalTaxNumber ? " ,federalTaxNumber='" + federalTaxNumber + "'" : ''
        query += drivesFor ? " ,drivesFor=" + drivesFor + "" : ''

        query += "where driverId=" + driverId;
        var request = new sql.Request();
        request.query(query, function (err, result) {
            if (err) {
                res.json({ success: false, err: err.message, message: 'Record not updated' })
            }
            else {
                res.json({ success: true, message: 'Record updated Successfully' })
            }
        });
    } catch (error) {
        res.json({ success: false, err: error.message, message: 'Record not Updated' })
    }
}


module.exports = {
    Provinces,
    Divisions,
    Terminals,
    Categories,
    Countries,
    Classes,
    Roles,
    SatelliteProviders,
    TravelDocuments,
    InsertDriver,
    ComplianceGroups,
    ComplianceGroupItems,
    DriverCompliancy,
    UpdateDriverCompliance,
    Drivers,
    Driver,
    UpdateDriver,
    ProvinceById,
    CountryByCode,
    InsertComplianceItems,
    DriverCompliancyGroup
}
const sql = require("mssql");
const PCMilerController = require('./pcmiler.controller')

const InsertCheckCall = async (req, res) => {
    try {
        var { tripId, nextEventId, latitude, longitude, currentCity, currentState } = req.body;
        // { "tripId": "83215", "eventId": "942907.0000", "latitude": "42.998290000", "longitude": "-82.437920000" }
        var request = new sql.Request();
        var nextLocation = await (await request.query("select l.postalCode,j.country from location l join Jurisdiction j on j.jurid=l.province where LocID=(select locationid from EventLog where id = " + nextEventId + ") order by locid desc")).recordset;
        if (nextLocation[0]) {
            var { lat, lon } = await PCMilerController.PostalCodeSearch(nextLocation[0].country, nextLocation[0].postalCode);
            if (lat, lon) {
                var milesOut = await PCMilerController.CalculateMiles(longitude + ',' + latitude, lon + ',' + lat);
                var timeRequired = milesOut ? (+milesOut / 50).toFixed(0) : null;
                if (milesOut && timeRequired) {
                    await (await request.query("DECLARE @CheckcallId integer,@DriverOnTime smallint;set @DriverOnTime=(select case when (select el.etaDate from EventLogDetail eld join EventLog el on el.UID=eld.UID where el.ID=" + nextEventId + " )>( DATEADD(MINUTE,@Minutes,GETDATE())) then 1 else 0 end);insert into EventCheckCallDetail(CheckCallDate,DriverOnTime,CityState, DistanceNextStop,DistanceMeasure,ETANextStop,SetTempMeasure,BoxTempMeasure,TripID) values(getdate(),@DriverOnTime,CONCAT('" + currentCity + "', ', ','" + currentState + "')," + milesOut + ",'M',DATEADD(MINUTE," + timeRequired + ",GETDATE()),'F','F'," + tripId + ") SET @CheckcallId=SCOPE_IDENTITY();update Trip set LastCheckCallUID=@CheckcallId where TripID=" + tripId + "")).recordset;

                }
            }

        }
        return true;
    } catch (error) {

    }
}

module.exports = {
    InsertCheckCall
}
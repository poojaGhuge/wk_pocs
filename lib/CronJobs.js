
/**
  * Created by hp on 06/03/2019.
  * function to fetch the data from Fleet Manger site
  * @param {*} callback
  * @author Yogesh Desai
  */
!(function () {
  'use strict'
  var moment = require('moment');
  var async = require('async');
  var sql = require("mssql/msnodesqlv8");
  var _ = require('underscore');
  const pgDB = require('../db-conn');
  // const pool = new Pool({
  //   user: 'postgres',
  //   host: 'localhost',
  //   database: 'WKTH_db',
  //   password: 'System123',
  //   port: 5432,
  // });
  var config = {
    user: 'wheelking0\\tg.arjun',
    server: 'WK-SQLDEV',
    database: 'FleetManager',
    driver: "msnodesqlv8",
    port:1433 ,
    pool: {
      max: 100,
      min: 0,
      idleTimeoutMillis: 30000
    },
    options: {
      trustedConnection: true
    },
    connectionTimeout: 30000,
    requestTimeout: 30000
  };
  var CronJobs = {
    getfleetMangerDataConnect: async function (callback) {
      sql.connect(config, function (err, data) {
        if (err) {
          console.log(err);
        } else {
          console.log(data);
        }
      })
    },

    getfleetMangerDataCronJob: async function (callback) {
      let request = new sql.Request();
      let today = moment(new Date()).subtract(7, 'days').format('YYYY-MM-DD hh:mm:ss');
      let nextSevenDay = moment(new Date()).add(7, 'days').format('YYYYMMDD');
      console.log('Script is running for ' + today + ' to ' + nextSevenDay);
      async.parallel([
        //   User table fleet manager
        function (callback) {
          async.series([
            function (cb) {
              request.query("SELECT * from [User] where Created >= '" + today + "'", function (err, tripData) {
                if (err) {
                  sql.close();
                  CronJobs.getfleetMangerDataConnect();
                  console.log('SQL Query Error01: ');
                  cb(err)
                } else {
                  _.forEach(tripData.recordset, function (datarecord) {
                    if (datarecord) {
                      let createddate = moment(datarecord.Created).format('YYYY-MM-DD h:mm:ss');
                      pgDB.pool.query("INSERT INTO tbluser(userid,username,realname,email,phone,lastlogon,password,createdon)VALUES(" + datarecord.UserID + ",'" + datarecord.UserName + "','" + datarecord.RealName + "','" + datarecord.Email + "','" + datarecord.UserPhoneExtension + "'," + datarecord.LastLogon + ",'admin123','" + createddate + "')",
                        (error, results) => {
                          if (error) {
                            if (error.code != '23505') {
                              console.log(error);
                            }
                          } else {
                            console.log('User added ');
                          }
                        })
                    }
                  });
                }
              });
            },
          ], function (err, result) {
            if (err) {
              callback(err);
            } else {
              callback(null, result);
            }
          })
        },
        //   Driver table fleet manager
        function (callback) {
          async.series([
            function (cb) {
              request.query("SELECT * from [Driver] where CreationDate>='" + today + "'", function (err, tripData) {
                if (err) {
                  sql.close();
                  CronJobs.getfleetMangerDataConnect();
                  console.log('SQL Query Error01: ');
                  cb(err)
                } else {
                  _.forEach(tripData.recordset, function (datarecord) {
                    if (datarecord) {
                      let createddate = moment(datarecord.CreationDate).format('YYYY-MM-DD hh:mm:ss');
                      pgDB.pool.query('INSERT INTO tbldriver(driverid,drvcode, firstname, lastname,status,email, phone, locationid,createdby,createdon) VALUES(' + datarecord.DriverID + ",'" + datarecord.DrvCode + "','" + datarecord.FirstName + "','" + datarecord.LastName + "','" + datarecord.Status + "','" + datarecord.EMailAddress + "','" + datarecord.Cellular + "'," + datarecord.LocationID + ",'" + datarecord.CreatedBy + "','" + createddate + "')", (error, results) => {
                        if (error) {
                          if (error.code != '23505') {
                            console.log(error);
                          }
                        } else {
                          console.log('Driver added ');
                        }
                      })
                    }
                  });
                }
              });
            },
          ], function (err, result) {
            if (err) {
              callback(err);
            } else {
              callback(null, result);
            }
          })
        }
      ])
    },

    updateTruckTrailerCronJob: async function (callback) {
      let request = new sql.Request();
      let today = moment(new Date()).subtract(7, 'days').format('YYYY-MM-DD hh:mm:ss');
      let nextSevenDay = moment(new Date()).add(7, 'days').format('YYYYMMDD');
      console.log('Script is running for ' + today + ' to ' + nextSevenDay);
      async.parallel([
        //   Trailer table fleet manager
        function (callback) {
          async.series([
            function (cb) {
              request.query("SELECT * from [Trailer]  where CreationDate>='" + today + "'", function (err, tripData) {
                if (err) {
                  sql.close();
                  CronJobs.getfleetMangerDataConnect();
                  console.log('SQL Query Error01: ');
                  cb(err)
                } else {
                  _.forEach(tripData.recordset, function (datarecord) {
                    ; if (datarecord) {
                      pgDB.pool.query("INSERT INTO tbltrailer(trailerid, state, locationid, chassis, make, model, modelyear, licenseplate, length, insideheight, doorheight, axles,airride, sidedoor, engineserial, enginemake, enginemodel, engineyear,unitno) values(" + datarecord.TrailerID + ",'" + datarecord.State + "'," + datarecord.LocationID + "," + datarecord.Chassis + ",'" + datarecord.Make + "','" + datarecord.Model + "'," + datarecord.ModelYear + ",'" + datarecord.LicensePlate + "'," + datarecord.Length + "," + datarecord.InsideHeight + "," + datarecord.DoorHeight + "," + datarecord.Axles + "," + datarecord.AirRide + "," + datarecord.SideDoor + ",'" + datarecord.EngineSerial + "','" + datarecord.EngineMake + "','" + datarecord.EngineModel + "'," + datarecord.EngineYear + ",'" + datarecord.UnitNo + "')", (error, results) => {
                        if (error) {
                          if (error.code != '23505') {
                            console.log(error);
                          }
                        } else {
                          console.log('Trailer added ');
                        }
                      })
                    }
                  });
                }
              });
            },
          ], function (err, result) {
            if (err) {
              callback(err);
            } else {
              callback(null, result);
            }
          })
        },
        //   Truck table fleet manager
        function (callback) {
          async.series([
            function (cb) {
              var qre = "select  t.TruckID,t.Status,t.state,t.LocationID,t.UnitNo,t.Model,t.ModelYear,t.LicensePlate,t.AssignedUser, " +
                " t.Odometer,t.OdometerType,t.Loc_ETA,t.NextETA,t.Event,t.NextEvent,l.City " +
                " from Truck t INNER JOIN Location l ON l.LocID = t.LocationID";
              // "SELECT t.Odometer,t.OdometerType,t.Loc_ETA,t.NextETA,t.Event,t.NextEvent,t.LocationID,l.City,* FROM Truck where CreationDate>='" + today + "'"
              request.query(qre, function (err, tripData) {
                if (err) {
                  sql.close();
                  CronJobs.getfleetMangerDataConnect();
                  console.log('SQL Query Error01: ');
                  cb(err)
                } else {
                  _.forEach(tripData.recordset, function (datarecord) {
                    if (datarecord) {
                      let loc_ETA = moment(datarecord.Loc_ETA).format('YYYY-MM-DD h:mm:ss');;
                      let createddate = moment(datarecord.CreationDate).format('YYYY-MM-DD h:mm:ss');
                      if (loc_ETA == "Invalid date") {
                        loc_ETA = moment().format('YYYY-MM-DD h:mm:ss');
                      }
                      if (createddate == "Invalid date") {
                        createddate = moment().format('YYYY-MM-DD h:mm:ss');
                      }
                      let qre = "INSERT INTO tbltruck(truckid,status,locationid,model,modelyear,licenseplate,assigneduser," +
                        " creationdate,currentstatus,unitno,odometer,odometertype,lasteta,city) " +
                        " values(" + datarecord.TruckID + ",'" + datarecord.Status + "'," +
                        " " + datarecord.LocationID + ",'" + datarecord.Model + "'," + datarecord.ModelYear + "," +
                        " '" + datarecord.LicensePlate + "'," + datarecord.AssignedUser + ",'" + createddate + "'," +
                        " '" + datarecord.CurrentStatus + "','" + datarecord.UnitNo + "'," + datarecord.Odometer + ",'" +
                        " " + datarecord.OdometerType + "','" + loc_ETA + "','" + datarecord.City + "')";
                      pgDB.pool.query(qre, (error, results) => {
                        if (error) {
                          if (error.code != '23505') {
                            console.log(error);
                          } else {
                            let updateQre = "UPDATE tbltruck " +
                              " SET status='" + datarecord.Status + "',state='" + datarecord.State + "'," +
                              " locationid=" + datarecord.LocationID + ",model='" + datarecord.Model + "'," +
                              " modelyear=" + datarecord.ModelYear + "," +
                              " licenseplate='" + datarecord.LicensePlate + "'," +
                              " assigneduser=" + datarecord.AssignedUser + "," +
                              " creationdate='" + createddate + "',currentstatus='" + datarecord.CurrentStatus + "'," +
                              " unitno='" + datarecord.UnitNo + "'," +
                              " odometer=" + datarecord.Odometer + "," +
                              " odometertype=" + datarecord.OdometerType + "," +
                              " Last_ETA='" + loc_ETA + "'," +
                              " city='" + datarecord.City + "'," +
                              " WHERE truckid=" + datarecord.TruckID + "" +
                              pgDB.pool.query(updateQre, (error, results) => {
                                if (error) {
                                  console.log(error);
                                } else {
                                  console.log('Truck Updated');
                                }
                              });
                          }
                        } else {
                          console.log('Truck added ');
                        }
                      })
                    }
                  });
                }
              });
            },
          ], function (err, result) {
            if (err) {
              callback(err);
            } else {
              callback(null, result);
            }
          })
        },

      ])
    },

    getCompliancyCronJob: async function (callback) {
      let request = new sql.Request();
      let today = moment(new Date()).subtract(7, 'days').format('YYYY-MM-DD hh:mm:ss');
      let nextSevenDay = moment(new Date()).subtract(7, 'days').format('YYYYMMDD');
      console.log('Script is running for ' + today + ' to ' + nextSevenDay);
      async.parallel([
        //   Trailer compliancy table fleet manager
        function (callback) {
          async.series([
            function (cb) {
              request.query("SELECT * FROM TrailerCompliancy where TrailerCompliancy.ExpiryDate is Not Null", function (err, tripData) {
                if (err) {
                  sql.close();
                  CronJobs.getfleetMangerDataConnect();
                  console.log('SQL Query Error01: ');
                  cb(err)
                } else {
                  var ExpiryDate;
                  _.forEach(tripData.recordset, function (datarecord) {
                    if (datarecord) {
                      if (datarecord.ExpiryDate) {
                        ExpiryDate = moment(datarecord.ExpiryDate).format('YYYY-MM-DD h:mm:ss');
                      } else {
                        ExpiryDate = datarecord.ExpiryDate;
                      }
                      datarecord.Item = datarecord.Item.replace(/[&\/\\#,+()$~%.'":*?<>{}]/g, '');
                      pgDB.pool.query("INSERT INTO tbltrailercompliancy(trailerid, items, description, expirydate) values(" + datarecord.TrailerID + ",'" + datarecord.Item + "','" + datarecord.Description + "','" + ExpiryDate + "')", (error, results) => {
                        if (error) {
                          if (error.code != '23505') {
                            console.log(error);
                          }
                        } else {
                          console.log('Trailer compliancy added ');
                        }
                      })
                    }
                  });
                }
              });
            },
          ], function (err, result) {
            if (err) {
              callback(err);
            } else {
              callback(null, result);
            }
          })
        },
        //   Truck Compliancy table fleet manager
        function (callback) {
          async.series([
            function (cb) {
              request.query("SELECT * FROM TruckCompliancy where TruckCompliancy.ExpiryDate is Not Null", function (err, tripData) {
                if (err) {
                  sql.close();
                  CronJobs.getfleetMangerDataConnect();
                  console.log('SQL Query Error01: ');
                  cb(err)
                } else {
                  _.forEach(tripData.recordset, function (datarecord) {
                    if (datarecord) {
                      let ExpiryDate = moment(datarecord.ExpiryDate).format('YYYY-MM-DD h:mm:ss');
                      datarecord.Item = datarecord.Item.replace(/[&\/\\#,+()$~%.'":*?<>{}]/g, '');
                      pgDB.pool.query("INSERT INTO tbltruckcompliancy(TruckID,Items, Description, ExpiryDate) values(" + datarecord.TruckID + ",'" + datarecord.Item + "','" + datarecord.Description + "','" + ExpiryDate + "')", (error, results) => {
                        if (error) {
                          if (error.code != '23505') {
                            console.log(error);
                          }
                        } else {
                          console.log('Truck compliancy added ');
                        }
                      })
                    }
                  });
                }
              });
            },
          ], function (err, result) {
            if (err) {
              callback(err);
            } else {
              callback(null, result);
            }
          })
        },
      ])
    },

    getLocationEventTypeCronJob: async function (callback) {
      let request = new sql.Request();
      let today = moment(new Date()).subtract(7, 'days').format('YYYY-MM-DD hh:mm:ss');
      let nextSevenDay = moment(new Date()).subtract(7, 'days').format('YYYYMMDD');
      console.log('Script is running for ' + today + ' to ' + nextSevenDay);
      async.parallel([
        //   location table fleet manager
        function (callback) {
          async.series([
            function (cb) {
              request.query("select l.*,j.Name as provinceName from [location] l ,[Jurisdiction] j where j.JurID = l.Province and l.CreationDate>='" + today + "'", function (err, tripData) {
                if (err) {
                  sql.close();
                  CronJobs.getfleetMangerDataConnect();
                  console.log('SQL Query Error01: ');
                  cb(err)
                } else {
                  _.forEach(tripData.recordset, function (datarecord) {
                    if (datarecord) {
                      datarecord.Address1 = datarecord.Address1.replace(/[&\/\\#,+()$~%.'":*?<>{}]/g, '');
                      datarecord.Address1 = datarecord.Address1.replace(/[&\/\\#,+()$~%.'":*?<>{}]/g, '');
                      datarecord.Location = datarecord.Location.replace(/[&\/\\#,+()$~%.'":*?<>{}]/g, '');
                      datarecord.provinceName = datarecord.provinceName.replace(/[&\/\\#,+()$~%.'":*?<>{}]/g, '');
                      datarecord.City = datarecord.City.replace(/[&\/\\#,+()$~%.'":*?<>{}]/g, '');
                      pgDB.pool.query("INSERT INTO tbllocation(locid,location_name,address1,address2,province,city,postalcode,contact,phone,emailaddress,latitude,longitude) values(" + datarecord.LocID + ",'" + datarecord.Location + "','" + datarecord.Address1 + "','" + datarecord.Address2 + "','" + datarecord.provinceName + "','" + datarecord.City + "','" + datarecord.PostalCode + "','" + datarecord.Contact + "','" + datarecord.Phone + "','" + datarecord.EMailAddress + "'," + datarecord.Latitude + "," + datarecord.Longitude + ")", (error, results) => {
                        if (error) {
                          if (error.code != '23505') {
                            console.log(error);
                          }
                        } else {
                          console.log('Location added ');
                        }
                      })
                    }
                  });
                }
              });
            },
          ], function (err, result) {
            if (err) {
              callback(err);
            } else {
              callback(null, result);
            }
          })
        },
        //   EventType table fleet manager
        function (callback) {
          async.series([
            function (cb) {
              request.query("SELECT * from [EventType]", function (err, tripData) {
                if (err) {
                  sql.close();
                  CronJobs.getfleetMangerDataConnect();
                  console.log('SQL Query Error01: ');
                  cb(err)
                } else {
                  _.forEach(tripData.recordset, function (datarecord) {
                    if (datarecord) {
                      pgDB.pool.query("INSERT INTO tbleventtype(id,description) values(" + datarecord.ID + ",'" + datarecord.Description + "')", (error, results) => {
                        if (error) {
                          if (error.code != '23505') {
                            console.log(error);
                          }
                        } else {
                          console.log('EventType added ');
                        }
                      })
                    }
                  });
                }
              });
            },
          ], function (err, result) {
            if (err) {
              callback(err);
            } else {
              callback(null, result);
            }
          })
        },
      ])
    },

    updatetripCronJob: async function (callback) {
      let request = new sql.Request();
      let today = moment(new Date()).subtract(1, 'days').format('YYYYMMDD');
      let nextTwoDay = moment(new Date()).add(3, 'days').format('YYYYMMDD');
      async.parallel([
        //   Trip table fleet manager
        function (callback) {
          async.series([
            function (cb) {
              var qre = "select  distinct e.ProID, t.TripID,t.DispatcherID,t.Driver1ID,e.TruckID,e.Trailer1ID,t.DateOut,t.LastEvent,t.NextEvent from Trip t " +
                "INNER JOIN EventLog e ON  e.TripID = t.TripID where t.DateOut between '" + today + "' and '" + nextTwoDay + "' and e.ProID is not null order by t.DateOut asc";
              // "INNER JOIN ProbillCosts ps ON  ps.TripID = t.TripID " +
              // "INNER JOIN Probill p ON  p.ProbillNo = ps.ProbillNo " +
              request.query(qre, function (err, tripData) {
                if (err) {
                  sql.close();
                  CronJobs.getfleetMangerDataConnect();
                  console.log('SQL Query Error01: ');
                  cb(err)
                } else {
                  _.forEach(tripData.recordset, function (datarecord) {
                    if (datarecord) {
                      let startddate = moment(datarecord.DateOut).format('YYYY-MM-DD h:mm:ss');
                      let createddate = moment(datarecord.Created).format('YYYY-MM-DD h:mm:ss');
                      let qre = "INSERT INTO tbltrip(tripid,dispatcherid,driverid,truckid,trailerid,startdate,event,nextevent,createddate) " +
                        " SELECT " + datarecord.TripID + "," + datarecord.DispatcherID + "," + datarecord.Driver1ID + "," + datarecord.TruckID + "," + datarecord.Trailer1ID + ", " +
                        " '" + startddate + "','" + datarecord.LastEvent + "','" + datarecord.NextEvent + "','" + createddate + "' " +
                        " WHERE NOT EXISTS (SELECT tripid FROM tbltrip WHERE tripid=" + datarecord.TripID + ")";
                      //'INSERT INTO tbltrip(tripid,dispatcherid,driverid,truckid,trailerid,startdate,event,nextevent,createddate) values(' + datarecord.TripID + "," + datarecord.DispatcherID + "," + datarecord.Driver1ID + "," + datarecord.TruckID + "," + datarecord.Trailer1ID + ", '" + startddate + "','" + datarecord.LastEvent + "','" + datarecord.NextEvent + "','" + createddate + "')"
                      pgDB.pool.query(qre, (error, results) => {
                        if (error) {
                          if (error.code != '23505') {
                            console.log(error);
                          }
                        } else {
                          console.log('Trip added ');
                        }
                      });
                    }

                  });
                }
              });
            },
          ], function (err, result) {
            if (err) {
              callback(err);
            } else {
              callback(null, result);
            }
          })
        },
      ])
    },

    updateProbillCronJob: async function (callback) {
      let request = new sql.Request();
      let today = moment(new Date()).subtract(1, 'days').format('YYYYMMDD');
      let nextTwoDay = moment(new Date()).add(2, 'days').format('YYYYMMDD');
      async.parallel([
        //   Probill table fleet manager
        function (callback) {
          async.series([
            function (cb) {
              var qr = "select distinct e.ProID,t.DateOut,e.TripID,e.TruckID,e.Trailer1ID,p.Pick_Date1,e.Start,p.Status, " +
                " p.ShipperLocID,p.PickupLocID,p.Pick_SignedBy,p.ConsigneeLocID,p.DeliveryLocID,p.Del_SignedBy from Probill p " +
                " INNER JOIN EventLog e ON e.ProID = p.ProbillNo INNER JOIN Trip t ON  t.tripid = e.tripid " +
                " where e.tripid is not null and t.DateOut >= '" + today + "' order by t.DateOut asc";
              // >=  between '" + today + "' and '" + nextTwoDay + "'
              request.query(qr, function (err, tripData) {
                if (err) {
                  sql.close();
                  CronJobs.getfleetMangerDataConnect();
                  console.log('SQL Query Error01: ');
                  cb(err)
                } else {
                  _.forEach(tripData.recordset, function (datarecord) {
                    if (datarecord) {
                      // let startdate = '';
                      // if (datarecord.Start) {
                      //   startdate = moment(datarecord.Start).format('YYYY-MM-DD h:mm:ss');
                      // } else {
                      //   startdate = datarecord.Start;
                      // }
                      let Pick_Date = moment(datarecord.Pick_Date1).format('YYYY-MM-DD h:mm:ss');
                      var qe = "INSERT INTO tblprobill(probillno, status,truckid,trailerid, tripid,shipperlocid, pickuplocid, pick_date, pick_signedby, consigneelocid, deliverylocid, del_signedby)" +
                        " SELECT " + datarecord.ProID + ",'" + datarecord.Status + "'," + datarecord.TruckID + "," + datarecord.Trailer1ID + "," + datarecord.TripID + "," + datarecord.ShipperLocID + "," +
                        " " + datarecord.PickupLocID + ",'" + Pick_Date + "','" + datarecord.Pick_SignedBy + "','" + datarecord.ConsigneeLocID + "', '" + datarecord.DeliveryLocID + "','" + datarecord.Del_SignedBy + "'" +
                        " WHERE NOT EXISTS (SELECT probillno,tripid FROM tblprobill WHERE probillno= " + datarecord.ProID + " AND tripid=" + datarecord.TripID + ")";
                      pgDB.pool.query(qe, (error, results) => {
                        if (error) {
                          if (error.code != '23505') {
                            console.log(error);
                          }
                        } else {
                          console.log('Probill added ');
                        }
                      })
                    }
                  });
                }
              });
            },
          ], function (err, result) {
            if (err) {
              callback(err);
            } else {
              callback(null, result);
            }
          })
        },
      ])
    },

    tableSpecificCronJob: async function (callback) {
      let request = new sql.Request();
      let today = moment(new Date()).subtract(7, 'days').format('YYYY-MM-DD h:mm:ss');
      let nextSevenDay = moment(new Date()).subtract(7, 'days').format('YYYYMMDD');
      console.log('Script is running for ' + today + ' to ' + nextSevenDay);
      async.parallel([
        //   Trailer compliancy table fleet manager
        function (callback) {
          async.series([
            function (cb) {
              request.query("SELECT * FROM TrailerCompliancy where TrailerCompliancy.ExpiryDate is Not Null", function (err, tripData) {
                if (err) {
                  sql.close();
                  CronJobs.getfleetMangerDataConnect();
                  console.log('SQL Query Error01: ');
                  cb(err)
                } else {
                  var ExpiryDate;
                  _.forEach(tripData.recordset, function (datarecord) {
                    if (datarecord) {
                      if (datarecord.ExpiryDate) {
                        ExpiryDate = moment(datarecord.ExpiryDate).format('YYYY-MM-DD h:mm:ss');
                      } else {
                        ExpiryDate = datarecord.ExpiryDate;
                      }
                      datarecord.Item = datarecord.Item.replace(/[&\/\\#,+()$~%.'":*?<>{}]/g, '');
                      pgDB.pool.query("INSERT INTO tbltrailercompliancy(trailerid, items, description, expirydate) values(" + datarecord.TrailerID + ",'" + datarecord.Item + "','" + datarecord.Description + "','" + ExpiryDate + "')", (error, results) => {
                        if (error) {
                          if (error.code != '23505') {
                            console.log(error);
                          }
                        } else {
                          console.log('Trailer added ');
                        }
                      })
                    }
                  });
                }
              });
            },
          ], function (err, result) {
            if (err) {
              callback(err);
            } else {
              callback(null, result);
            }
          })
        },
        //   Truck Compliancy table fleet manager
        function (callback) {
          async.series([
            function (cb) {
              request.query("SELECT * FROM TruckCompliancy where TruckCompliancy.ExpiryDate is Not Null", function (err, tripData) {
                if (err) {
                  sql.close();
                  CronJobs.getfleetMangerDataConnect();
                  console.log('SQL Query Error01: ');
                  cb(err)
                } else {
                  _.forEach(tripData.recordset, function (datarecord) {
                    if (datarecord) {
                      let ExpiryDate = moment(datarecord.ExpiryDate).format('YYYY-MM-DD h:mm:ss');
                      datarecord.Item = datarecord.Item.replace(/[&\/\\#,+()$~%.'":*?<>{}]/g, '');
                      let qr = "INSERT INTO tbltruckcompliancy(TruckID,Items, Description, ExpiryDate) values(" + datarecord.TruckID + ",'" + datarecord.Item + "','" + datarecord.Description + "','" + ExpiryDate + "')";
                      pgDB.pool.query("INSERT INTO tbltruckcompliancy(TruckID,Items, Description, ExpiryDate) values(" + datarecord.TruckID + ",'" + datarecord.Item + "','" + datarecord.Description + "','" + ExpiryDate + "')", (error, results) => {
                        if (error) {
                          if (error.code != '23505') {
                            console.log(error);
                          }
                        } else {
                          console.log('Truck added ');
                        }
                      })
                    }
                  });
                }
              });
            },
          ], function (err, result) {
            if (err) {
              callback(err);
            } else {
              callback(null, result);
            }
          })
        },
      ])
    }
  }
  module.exports = CronJobs;
})();


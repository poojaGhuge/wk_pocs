var express = require('express');
var router = express.Router()
var loadTender = require('../controllers/load-tender.controller');

// router.use(jwtmiddleware.checkToken);

router.get('/getCommentsOnTrip', loadTender.getCommentsOnTrip);
router.get('/paymentTerms', loadTender.paymentTerms);
router.get('/categories', loadTender.categories);
router.get('/customBrokers/:searchTerm', loadTender.customBrokers);
router.get('/pickupScheduleTypes', loadTender.pickupScheduleTypes);
router.get('/deliveryScheduleTypes', loadTender.deliveryScheduleTypes);
router.get('/divisions', loadTender.divisions);
router.get('/salesReps', loadTender.salesReps);
router.get('/currencies', loadTender.currencies);
router.get('/billingCompany/:customerId', loadTender.billingCompany);
router.get('/locations/:searchTerm', loadTender.locations);
router.get('/customer/:searchTerm', loadTender.customer);
router.get('/equipmentTypes', loadTender.equipmentTypes);
router.post('/insertLoadTender', loadTender.insertLoadTender);
router.get('/units', loadTender.units);
router.get('/accChargeTypes', loadTender.accChargeTypes);
router.get('/checkOrderExists/:poNo/:customerId', loadTender.checkOrderExists);



module.exports = router;
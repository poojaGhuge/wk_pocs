let jwtmiddleware = require('../jwt/jwt.controller');
var express = require('express');
var router = express.Router()
var reportsController = require('../controllers/reports-controller');

// router.use(jwtmiddleware.checkToken);

router.get('/getCommentsOnTrip', reportsController.getCommentsOnTrip);



module.exports = router;
var express = require('express');
var router = express.Router()
const readPdfController = require('../controllers/readPdf.controller');

router.get('/', readPdfController.sendPdf);
router.get('/readCSV', readPdfController.readCSV);



module.exports = router;
var express = require('express');
const carrierController = require('../controllers/carrier.controller');
var router = express.Router()

router.get('/Categories', carrierController.Categories);
router.post('/InsertCarrier', carrierController.InsertCarrier);
router.get('/Carriers', carrierController.Carriers);
router.put('/UpdateCarrier', carrierController.UpdateCarrier);

module.exports = router;
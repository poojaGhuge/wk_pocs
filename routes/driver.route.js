var express = require('express');
const driverController = require('../controllers/driver.controller');
var router = express.Router()

router.get('/Provinces/:searchTerm', driverController.Provinces);
router.get('/ProvinceById/:jurId', driverController.ProvinceById);
router.get('/Divisions', driverController.Divisions);
router.get('/Terminals', driverController.Terminals);
router.get('/Categories', driverController.Categories);
router.get('/Countries/:searchTerm', driverController.Countries);
router.get('/CountryByCode/:countryCode', driverController.CountryByCode);
router.get('/Classes', driverController.Classes);
router.get('/Roles', driverController.Roles);
router.get('/SatelliteProviders', driverController.SatelliteProviders);
router.get('/TravelDocuments', driverController.TravelDocuments);
router.get('/ComplianceGroups', driverController.ComplianceGroups);
router.get('/ComplianceGroupItems/:driverId', driverController.ComplianceGroupItems);
router.get('/DriverCompliancy/:driverId', driverController.DriverCompliancy);
router.get('/DriverCompliancyGroup/:driverId', driverController.DriverCompliancyGroup);
router.get('/Drivers', driverController.Drivers);
router.get('/Driver/:driverId', driverController.Driver);
router.post('/InsertDriver', driverController.InsertDriver);
router.put('/UpdateDriverCompliance', driverController.UpdateDriverCompliance);
router.put('/UpdateDriver', driverController.UpdateDriver);
router.post('/InsertComplianceItems', driverController.InsertComplianceItems);





module.exports = router;